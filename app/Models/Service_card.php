<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Service_card extends Model
{
    use HasFactory;

    public $timestamps = false;

    protected $table = 'service_card';
    protected $fillable = [

        'id',
        'image',
        'service_name',
        'service_text',
        'more'






    ];
}
