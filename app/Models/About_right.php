<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class About_right extends Model
{
    use HasFactory;

    public $timestamps = false;

    protected $table = 'about_right';
    protected $fillable = [
        'italic_text',
        'check_text',
        'simple_text',
        'video',
        'img_bg',


    ];
}
