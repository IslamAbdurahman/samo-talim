<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Introduction extends Model
{
    use HasFactory;

    public $timestamps = false;

    protected $table = 'intruduction';
    protected $fillable = [

        'id',
        'name',
        'text',
        'video',


    ];
}
