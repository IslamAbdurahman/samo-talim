<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class About_left extends Model
{
    use HasFactory;

    public $timestamps = false;

    protected $table = 'about_left';
    protected $fillable = [

        'id',
        'header_text',
        'image',
        'text_bottom',

    ];
}
