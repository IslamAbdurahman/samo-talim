<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Count extends Model
{
    use HasFactory;
    public $timestamps = false;

    protected $table = 'count';
    protected $fillable = [

        'id',
        'number',
        'bold_text',
        'simple_text',


    ];
}
