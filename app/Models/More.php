<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class More extends Model
{
    use HasFactory;

    public $timestamps = false;

    protected $table = 'more';
    protected $fillable = [

        'id',
        'name',
        'swipe_text',



    ];
}
