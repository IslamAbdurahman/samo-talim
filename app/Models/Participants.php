<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Participants extends Model
{
    use HasFactory;

    public $timestamps = false;

    protected $table = 'participants';
    protected $fillable = [

        'id',
        'card_image',
        'card_name',
        'job',
        'card_text',


    ];
}
