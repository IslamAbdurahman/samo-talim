<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Teachers extends Model
{
    use HasFactory;

    public $timestamps = false;

    protected $table = 'teachers';
    protected $fillable = [

        'id',
        'image',
        'teacher_name',
        'teacher_job',
        'telegram_icon',
        'instagram_icon',
        'facebook_icon',
        'twitter',
    ];
}
