<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Pricing extends Model
{
    use HasFactory;

    public $timestamps = false;

    protected $table = 'pricing';
    protected $fillable = [

        'id',
        'name',
        'image',
        'price_number',
        'correct_text',
        'incorrect_text',



    ];
}
