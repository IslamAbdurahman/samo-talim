<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use mysql_xdevapi\Table;

class Portfolio_category extends Model
{
    use HasFactory;
    public $timestamps = false;

    protected $table = 'portfolio_category';
    protected $fillable = [

        'id',
        'name',
    ];
}
