<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Home extends Model
{
    use HasFactory;

    public $timestamps = false;

    protected $table = 'home';
    protected $fillable = [

        'id',
        'header_name',
        'text',
        'photo',
        'video',


    ];
}
