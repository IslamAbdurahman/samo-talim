<?php

namespace App\Http\Controllers;

use App\Models\About_left;
use App\Models\Cards;
use Illuminate\Http\Request;

class CardsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $cards=Cards::all();
        return view('admin.pages.cards.index', compact('cards'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'text'=>'required',
            'image'=>'required',
            'link'=>'required'

        ]);

        $item= new Cards();
        $item->text=$request->text;
        $item->link=$request->link;

        $imageName = time().'.'.$request->file('image')->getClientOriginalExtension();
        $request->file('image')->storeAs('public/images/', $imageName);
        $item->image=$imageName;

        $item->save();
        return redirect()->route('cards.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'text'=>'required',
        ]);

        $update=Cards::find($id);
        $update->text=$request->text;
        $update->link=$request->link;
        if ($request->file('image')){
            $imageName = time().'.'.$request->file('image')->getClientOriginalExtension();
            $request->file('image')->storeAs('public/images/', $imageName);
            if (file_exists(public_path('storage/images/'.$update->image))){
                @unlink(public_path('storage/images/'.$update->image));
            }
            $update->image=$imageName;
        }

        $update->update();
        return redirect()->route('cards.index');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $delete=Cards::find($id);
        $delete->delete();
        return redirect()->route('cards.index');
    }
}
