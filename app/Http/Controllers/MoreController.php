<?php

namespace App\Http\Controllers;

use App\Models\More;

use App\Models\Participants;
use Illuminate\Http\Request;

class MoreController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $more = More::all();
        return view('admin.pages.more.index', compact('more'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([

            'name'=>'required',
            'swipe_text'=>'required',



        ]);

        More::create([
            'name'=> $request->name,
            'swipe_text'=>$request->swipe_text,

        ]);

        return redirect()->route('more.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([

            'name'=>'required',
            'swipe_text'=>'required',


        ]);
        $more = More::find($id);
        $more->name = $request->name;
        $more->swipe_text = $request->swipe_text;

        $more->update();

        return redirect()->route('more.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $more = More::find($id);
        $more->delete();

        return redirect()->route('more.index');
    }
}
