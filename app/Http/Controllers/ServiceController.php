<?php

namespace App\Http\Controllers;

use App\Models\Service;
use App\Models\Service_card;
use Illuminate\Http\Request;

class ServiceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $service = Service::find(1);
        return view('admin.pages.service.index', compact('service'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'image'=>'required',
            'number'=>'required',
            'bold_text'=>'required',
            'simple_text'=>'required',
        ]);

        if ($request->file('image')){
            $imageName = time().'.'.$request->file('image')->getClientOriginalExtension();
            $request->file('image')->storeAs('public/images/',$imageName);

        }else{
            $imageName='no image';
        }
        Service::create([
            'image'=> $imageName,
            'number'=>$request->number,
            'bold_text'=>$request->bold_text,
            'simple_text'=>$request->simple_text,

        ]);

        return redirect()->route('service.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([

            'number'=>'required',
            'bold_text'=>'required',
            'simple_text'=>'required',


        ]);
        $service = Service::find($id);
        $service->number = $request->number;
        $service->bold_text = $request->bold_text;
        $service->simple_text = $request->simple_text;




        if ($request->file('image')){
            $imageName = time().'.'.$request->file('image')->getClientOriginalExtension();
            $request->file('image')->storeAs('public/images/', $imageName);
            if (file_exists(public_path('storage/images/'.$service->image))){
                @unlink(public_path('storage/images/'.$service->image));
            }
            $service->image=$imageName;
        }

        $service->update();

        return redirect()->route('service.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $service = Service::find($id);
        $service->delete();

        return redirect()->route('service.index');
    }
}
