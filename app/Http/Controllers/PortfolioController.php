<?php

namespace App\Http\Controllers;

use App\Models\Participants;
use App\Models\Portfolio;
use App\Models\Portfolio_category;
use http\Message;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class PortfolioController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $portfolio = DB::select('SELECT p.*,pc.name as category from portfolio p JOIN portfolio_category pc on p.cat_id=pc.id');
        $categories = Portfolio_category::all();
        return view('admin.pages.portfolio.index', compact('portfolio', 'categories'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([

            'name'=>'required',
            'text'=>'required',



        ]);


        if ($request->file('image')){
            $imageName = time().'.'.$request->file('image')->getClientOriginalExtension();
            $request->file('image')->storeAs('public/images/',$imageName);

        }else{
            $imageName='no image';
        }
        Portfolio::create([
            'image'=> $imageName,
            'name'=>$request->name,
            'text'=>$request->text,
            'cat_id'=>$request->cat_id,
        ]);

        return redirect()->route('portfolio.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([

            'name'=>'required',
            'text'=>'required',
            'cat_id'=>'required',


        ]);
        $portfolio = Portfolio::find($id);
        $portfolio->name = $request->name;
        $portfolio->text = $request->text;
        $portfolio->cat_id = $request->cat_id;


        if ($request->file('image')){
            $imageName = time().'.'.$request->file('image')->getClientOriginalExtension();
            $request->file('image')->storeAs('public/images/', $imageName);
            if (file_exists(public_path('storage/images/'.$portfolio->image))){
                @unlink(public_path('storage/images/'.$portfolio->image));
            }

            $portfolio->image=$imageName;
        }

        $portfolio->update();

        return redirect()->route('portfolio.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $portfolio = Portfolio::find($id);
        if (file_exists(public_path('storage/images/'.$portfolio->image))){
            @unlink(public_path('storage/images/'.$portfolio->image));
        }
        $portfolio->delete();

        return redirect()->route('portfolio.index');
    }
}
