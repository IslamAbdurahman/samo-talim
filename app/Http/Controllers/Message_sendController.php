<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
class Message_sendController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $apiToken = "5811642290:AAFww9jnMGzUoMLnok_4vp55278nBWTvBIs";
        $data = [
            'chat_id' => '1967821230',
            'text' => 'Xabar:' . $request->message . '-->'

        ];
        echo "<br>";
        $data_2 = [
            'Telefon_raqam:' => $request->number . '-->'
        ];
        echo "<br>";
        $data_3 = [
            'Tuliq_ism:' => $request->name . '-->'
        ];
        echo "<br>";
        $data_4 = [
            'Mavzu:' => $request->subject
        ];
        echo "<br>";
        $response = file_get_contents("https://api.telegram.org/bot$apiToken/sendMessage?"
            . http_build_query($data)
            . http_build_query($data_2)
            . http_build_query($data_3)
            . http_build_query($data_4));

        return Redirect::back()->with('success', 'Sizning xabaringiz muvofaqiyatli yuborildi!!');
    }




    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
