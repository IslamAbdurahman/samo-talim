<?php

namespace App\Http\Controllers;

use App\Models\About_left;
use App\Models\About_right;
use App\Models\User;
use Illuminate\Auth\Notifications\ResetPassword;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Validation\Rules\Password;

class About_rightController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.pages.admin_data.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $credentials = $request->validate([
            'email'=>['required','max:40','string'],
              'password'=>[
                  'required',
                  Password::min(8)
                      ->letters()
                      ->mixedCase()
                      ->numbers()
                      ->symbols()
                      ->uncompromised()
                  ],
              'verifypass'=>['required']

        ]);

        $admin = Auth::user();
        if($admin->email == $request->email){
           $admin->password=Hash::make($request->password);
            $admin->save();
            return Redirect::back()->with('success', 'Ozgarish muvofaqiyatli saqlandi!!');

        }else{
            return Redirect::back()->with('failure', 'Amaliyot bajarilmadi. Kirgazgan malumotlaringizni tekshiring');


        }
        if($request->password !== $request->verifypass){
            return Redirect::back()->with('xato', 'Amaliyot bajarilmadi. Kirgazilgan parollar mos kelmadi');

        }







    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'italic_text'=>'required',
            'check_text'=>'required',
            'simple_text'=>'required',
            'video'=>'required',

        ]);

        $update=About_right::find($id);
        $update->italic_text=$request->italic_text;
        $update->check_text=$request->check_text;
        $update->simple_text=$request->simple_text;
        $update->video=$request->video;


        if ($request->file('img_bg')){
            $imageName = time().'.'.$request->file('img_bg')->getClientOriginalExtension();
            $request->file('img_bg')->storeAs('public/images/', $imageName);
            if (file_exists(public_path('storage/images/'.$update->img_bg))){
                @unlink(public_path('storage/images/'.$update->img_bg));
            }
            $update->img_bg=$imageName;
        }

        $update->update();
        return redirect()->route('about_left.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
