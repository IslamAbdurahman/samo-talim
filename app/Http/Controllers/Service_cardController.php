<?php

namespace App\Http\Controllers;

use App\Models\Pricing;
use App\Models\Service_card;
use Illuminate\Http\Request;

class Service_cardController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $service_card = Service_card::all();
        return view('admin.pages.service_card.index', compact('service_card'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'service_name'=>'required',
            'service_text'=>'required',
            'more'=>'required',

        ]);



        if ($request->file('image')){
            $imageName = time().'.'.$request->file('image')->getClientOriginalExtension();
            $request->file('image')->storeAs('public/images/',$imageName);

        }else{
            $imageName='no image';
        }
        Service_card::create([
            'image'=> $imageName,
            'service_name'=>$request->service_name,
            'service_text'=>$request->service_text,
            'more'=>$request->more


        ]);

        return redirect()->route('service_card.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([


            'service_name'=>'required',
            'service_text'=>'required',
            'more'=>'required',




        ]);
        $service_card = Service_card::find($id);
        $service_card->service_name = $request->service_name;
        $service_card->service_text = $request->service_text;
        $service_card->more = $request->more;





        if ($request->file('image')){
            $imageName = time().'.'.$request->file('image')->getClientOriginalExtension();
            $request->file('image')->storeAs('public/images/', $imageName);
            if (file_exists(public_path('storage/images/'.$service_card->image))){
                @unlink(public_path('storage/images/'.$service_card->image));
            }
            $service_card->image=$imageName;
        }

        $service_card->update();

        return redirect()->route('service_card.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $service_card = Service_card::find($id);
        if (file_exists(public_path('storage/images/'.$service_card->image))){
            @unlink(public_path('storage/images/'.$service_card->image));
        }
        $service_card->delete();

        return redirect()->route('service_card.index');
    }
}
