<?php

namespace App\Http\Controllers;

use App\Models\Portfolio;
use App\Models\Pricing;
use http\Exception\BadQueryStringException;
use Illuminate\Http\Request;

class PricingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $pricing = Pricing::all();
        return view('admin.pages.pricing.index', compact('pricing'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name'=>'required',
            'image'=>'required',
            'price_number'=>'required',
            'correct_text'=>'required',
            'incorrect_text'=>'required',
        ]);

        Pricing::create([
            'name'=> $request->name,
            'image'=>$request->image,
            'price_number'=>$request->price_number,
            'correct_text'=>$request->correct_text,
            'incorrect_text'=>$request->incorrect_text,

        ]);

        return redirect()->route('portfolio.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([

            'name'=>'required',
            'price_number'=>'required',
            'correct_text'=>'required',
            'incorrect_text'=>'required',



        ]);
        $pricing = Pricing::find($id);
        $pricing->name = $request->name;
        $pricing->price_number = $request->price_number;
        $pricing -> correct_text= $request->correct_text;
        $pricing->incorrect_text = $request->incorrect_text;







//        if ($request->file('image')){
//            $imageName = time().'.'.$request->file('image')->getClientOriginalExtension();
//            $request->file('image')->storeAs('public/images/', $imageName);
//            if (file_exists(public_path('storage/images/'.$pricing->image))){
//                @unlink(public_path('storage/images/'.$pricing->image));
//            }
//            $pricing->image=$imageName;
//        }

        $pricing->update();

        return redirect()->route('pricing.index');    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $pricing = Pricing::find($id);
        $pricing->delete();

        return redirect()->route('pricing.index');
    }
}
