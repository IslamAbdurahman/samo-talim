<?php

namespace App\Http\Controllers;



use App\Models\Participants;
use Illuminate\Http\Request;

class ParticipantsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $participants = Participants::all();

        return view('admin.pages.participants.index', compact('participants'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'card_name'=>'required',
            'job'=>'required',
            'card_text'=>'required',


        ]);
        if ($request->file('card_image')){
            $imageName = time().'.'.$request->file('card_image')->getClientOriginalExtension();
            $request->file('card_image')->storeAs('public/images/', $imageName);


        }else{
            $imageName='no image';
        }

        Participants::create([
            'card_image'=> $imageName,
            'card_name'=>$request->card_name,
            'job'=>$request->job,
            'card_text'=>$request->card_text,
        ]);



        return redirect()->route('participants.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'card_name'=>'required',
            'job'=>'required',
            'card_text'=>'required',

        ]);
        $participants = Participants::find($id);
//        $participants->card_image = $request->card_image;
        $participants->card_name = $request->card_name;
        $participants->job = $request->job;
        $participants->card_text = $request->card_text;


        if ($request->file('card_image')){
            $imageName = time().'.'.$request->file('card_image')->getClientOriginalExtension();
            $request->file('card_image')->storeAs('public/images/', $imageName);
            if (file_exists(public_path('storage/images/'.$participants->card_image))){
                @unlink(public_path('storage/images/'.$participants->card_image));
            }
            $participants->card_image=$imageName;
        }

        $participants->update();

        return redirect()->route('participants.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $participants = Participants::find($id);
        if (file_exists(public_path('storage/images/'.$participants->card_image))){
            @unlink(public_path('storage/images/'.$participants->card_image));
        }
        $participants->delete();

        return redirect()->route('participants.index');
    }
}
