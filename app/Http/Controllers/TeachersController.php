<?php

namespace App\Http\Controllers;

use App\Models\Service;
use App\Models\Teachers;
use Illuminate\Http\Request;

class TeachersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $teachers = Teachers::all();
        return view('admin.pages.teachers.index', compact('teachers'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'image'=>'required',
            'teacher_name'=>'required',
            'teacher_job'=>'required',
            'telegram_icon'=>'required',
            'instagram_icon'=>'required',
            'facebook_icon'=>'required',
            'twitter'=>'required',


        ]);


        if ($request->file('image')){
            $imageName = time().'.'.$request->file('image')->getClientOriginalExtension();
            $request->file('image')->storeAs('public/images/',$imageName);

        }else{
            $imageName='no image';
        }

        Teachers::create([
            'image'=> $imageName,
            'teacher_name'=>$request->teacher_name,
            'teacher_job'=>$request->teacher_job,
            'telegram_icon'=>$request->telegram_icon,
            'instagram_icon'=>$request->instagram_icon,
            'facebook_icon'=>$request->facebook_icon,
            'twitter'=>$request->twitter,


        ]);

        return redirect()->route('teachers.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([

            'teacher_name'=>'required',
            'teacher_job'=>'required',
            'telegram_icon'=>'required',
            'instagram_icon'=>'required',
            'facebook_icon'=>'required',
            'twitter'=>'required',



        ]);
        $teachers = Teachers::find($id);
        $teachers->teacher_name = $request->teacher_name;
        $teachers->teacher_job = $request->teacher_job;
        $teachers->telegram_icon = $request->telegram_icon;
        $teachers->instagram_icon = $request->instagram_icon;
        $teachers->facebook_icon = $request->facebook_icon;
        $teachers->twitter = $request->twitter;





        if ($request->file('image')){
            $imageName = time().'.'.$request->file('image')->getClientOriginalExtension();
            $request->file('image')->storeAs('public/images/', $imageName);
            if (file_exists(public_path('storage/images/'.$teachers->image))){
                @unlink(public_path('storage/images/'.$teachers->image));
            }
            $teachers->image=$imageName;
        }

        $teachers->update();

        return redirect()->route('teachers.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $teachers = Teachers::find($id);
        if (file_exists(public_path('storage/images/'.$teachers->image))){
            @unlink(public_path('storage/images/'.$teachers->image));
        }
        $teachers->delete();

        return redirect()->route('teachers.index');
    }
}
