<?php

namespace App\Http\Controllers;

use App\Models\Cards;
use App\Models\Home;
use Illuminate\Http\Request;

class HomeMainController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $home=Home::find(1);
        return view('admin.pages.home.index', compact('home'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'header_name'=>'required',
            'text'=>'required',
            'video'=>'required'
        ]);

        $update=Home::find($id);
        $update->header_name=$request->header_name;
        $update->video=$request->video;
        $update->text=$request->text;
        if ($request->file('photo')){
            $imageName = time().'.'.$request->file('photo')->getClientOriginalExtension();
            $request->file('photo')->storeAs('public/images/', $imageName);
            if (file_exists(public_path('storage/images/'.$update->photo))){
                @unlink(public_path('storage/images/'.$update->photo));
            }
            $update->photo=$imageName;
        }

        $update->update();
        return redirect()->route('home_main.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
