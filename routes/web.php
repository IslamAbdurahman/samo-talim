<?php

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
//    dd('salom');
    $about_left=\App\Models\About_left::find(1);
    $about_right=\App\Models\About_right::find(1);
    $card=\App\Models\Cards::all();
    $contact=\App\Models\Contact::find(1);
    $home=\App\Models\Home::find(1);
    $intro=\App\Models\Introduction::find(1);
    $message=\App\Models\Message::find(1);
    $more=\App\Models\More::all();
    $part=\App\Models\Participants::all();
    $portfolio=DB::select('SELECT p.*,pc.name as category from portfolio p JOIN portfolio_category pc on p.cat_id=pc.id');
    $pricing=\App\Models\Pricing::all();
    $service=\App\Models\Service::all();
    $service_card=\App\Models\Service_card::all();
    $teacher=\App\Models\Teachers::all();

    return view('samo.index.index', compact(

        'about_left', 'about_right','card',
        'contact','home','intro','message','more','part','portfolio',
        'pricing','service','service_card','teacher'

    ));
})->name('samo.home');
Route::get('admin', function () {
    return view('admin.layouts.app');
});
Route::middleware(['auth'])->group(function () {
    Route::resource('about_left',\App\Http\Controllers\About_leftController::class);
    Route::resource('about_right',\App\Http\Controllers\About_rightController::class);
    Route::resource('blog',\App\Http\Controllers\BlogController::class);
    Route::resource('message',\App\Http\Controllers\MessageController::class);
    Route::resource('more',\App\Http\Controllers\MoreController::class);
    Route::resource('portfolio',\App\Http\Controllers\PortfolioController::class);
    Route::resource('pricing',\App\Http\Controllers\PricingController::class);
    Route::resource('cards',\App\Http\Controllers\CardsController::class);
    Route::resource('contact',\App\Http\Controllers\ContactController::class);
    Route::resource('home_main',\App\Http\Controllers\HomeMainController::class);
    Route::resource('introduction',\App\Http\Controllers\IntroductionController::class);
    Route::resource('participants',\App\Http\Controllers\ParticipantsController::class);
    Route::resource('service',\App\Http\Controllers\ServiceController::class);
    Route::resource('service_card',\App\Http\Controllers\Service_cardController::class);
    Route::resource('teachers',\App\Http\Controllers\TeachersController::class);
    Route::resource('count', \App\Http\Controllers\BlogController::class);

});
Route::resource('admin_data', \App\Http\Controllers\About_rightController::class);
Route::resource('sendmessage', \App\Http\Controllers\Message_sendController::class);




Auth::routes([
    'register' => false, // Registration Routes...
    'reset' => false, // Password Reset Routes...
    'verify' => false, // Email Verification Routes...
]);



//Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home.index');

