@extends('samo.layouts.app')


@section('main')


    <!-- ======= Header ======= -->
    <section id="topbar" class="topbar d-flex align-items-center">
        <div class="container d-flex justify-content-center justify-content-md-between">
            <div class="contact-info d-flex align-items-center">
                <i class="bi bi-envelope d-flex align-items-center"><a href="mailto:{{$contact->email}}">{{$contact->email}}</a></i>
                <i class="bi bi-phone d-flex align-items-center ms-4"><span>{{$contact->phone}}</span></i>
            </div>
            <div class="social-links d-none d-md-flex align-items-center">
                <a href="{{$contact->twitter}}" target="_blank" class="twitter"><i class="bi bi-twitter"></i></a>
                <a href="{{$contact->facebook}}" target="_blank" class="facebook"><i class="bi bi-facebook"></i></a>
                <a href="{{$contact->instagram}}" target="_blank" class="instagram"><i class="bi bi-instagram"></i></a>
                <a href="{{$contact->telegram}}" target="_blank" class="linkedin"><i class="bi bi-telegram"></i></a>
            </div>
        </div>
    </section><!-- End Top Bar -->


    <header id="header" class="header d-flex align-items-center">

        <div class="container-fluid container-xl d-flex align-items-center justify-content-between">
            <a href="index.html" class="logo d-flex align-items-center">
                <!-- Uncomment the line below if you also wish to use an image logo -->
                <!-- <img src="public/samo/assets/img/logo.png" alt=""> -->
                <div>
                    <img style="border-radius: 50%" src="/public/samo/samo_photo.jpg">
                </div>
                <h1>SAMO TA'LIM<span>.</span></h1>
            </a>
            <nav id="navbar" class="navbar">
                <ul>
                    <li><a href="#hero">Asosiy</a></li>
                    <li><a href="#about">Biz haqimizda</a></li>
                    <li><a href="#services">Bizning xizmatlar</a></li>
                    <li><a href="#portfolio">Fanlar</a></li>
                    <li><a href="#team">Bizning ustozlar</a></li>
                    <li><a href="#contact">Bog'lanish</a></li>
                </ul>
            </nav><!-- .navbar -->

            <i class="mobile-nav-toggle mobile-nav-show bi bi-list"></i>
            <i class="mobile-nav-toggle mobile-nav-hide d-none bi bi-x"></i>

        </div>
    </header><!-- End Header -->
    <!-- End Header -->

    <!-- ======= Hero Section ======= -->
    <section id="hero" class="hero">
        <div class="container position-relative">
            <div class="row gy-5" data-aos="fade-in">
                <div class="col-lg-6 order-2 order-lg-1 d-flex flex-column justify-content-center text-center text-lg-start">
                    <h2>{{$home->header_name}}</h2>
                    <p>{{$home->text}}</p>
                    <div class="d-flex justify-content-center justify-content-lg-start">
                        <a href="#about" class="btn-get-started">Boshlash</a>
                        <a href="{{$home->video}}" class="glightbox btn-watch-video d-flex align-items-center"><i class="bi bi-play-circle"></i><span>Video</span></a>
                    </div>
                </div>
                <div class="col-lg-6 order-1 order-lg-2">
                    <img src="{{asset('public/storage/images/'.$home->photo)}}" class="img-fluid" alt="" data-aos="zoom-out" data-aos-delay="100">
                </div>
            </div>
        </div>

        <div class="icon-boxes position-relative">
            <div class="container position-relative">
                <div class="row gy-4 mt-5">

                  @foreach($card as $cards)
                        <div class="col-xl-3 col-md-6" data-aos="fade-up" data-aos-delay="100">
                            <div class="icon-box">
                                <div class="icon"><img width="80px" height="80px" src="{{asset('public/storage/images/'.$cards->image)}}"></div>
                                <h4 class="title"><a href="{{$cards->link}}" target="_blank" class="stretched-link">{{$cards->text}}</a></h4>
                            </div>

                        </div>
                  @endforeach
                    <!--End Icon Box -->

                </div>
            </div>
        </div>

        </div>
    </section>
    <!-- End Hero Section -->

    <main id="main">

        <!-- ======= About Us Section ======= -->
        <section id="about" class="about">
            <div class="container" data-aos="fade-up">

                <div class="section-header">
                    <h2>Biz Haqimizda</h2>
                </div>

                <div class="row gy-4">
                    <div class="col-lg-6">
                        <h3>{{$about_left->header_text}}</h3>
                        <img src="{{asset('public/storage/images/'.$about_left->image)}}" class="img-fluid rounded-4 mb-4" alt="">
                        <p>{{$about_left->text_bottom}}</p>
                    </div>
                    <div class="col-lg-6">
                        <div class="content ps-0 ps-lg-5">
                            <p class="fst-italic">
                                {{$about_right->italic_text}}
                            </p>
                            <ul>
                                <li><i class="bi bi-check-circle-fill"></i>{{$about_right->check_text}}</li>
                            </ul>
                            <p>
                                {{$about_right->simple_text}}
                            </p>

                            <div class="position-relative mt-4">
                                <img src="{{asset('public/storage/images/'.$about_right->img_bg)}}" class="img-fluid rounded-4" alt="">
                                <a href="{{$about_right->video}}" class="glightbox play-btn"></a>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </section><!-- End About Us Section -->

        <!-- ======= Call To Action Section ======= -->
        <section id="call-to-action" class="call-to-action">
            <div class="container text-center" data-aos="zoom-out">
                <a href="{{$intro->video}}" class="glightbox play-btn"></a>
                <h3>{{$intro->name}}</h3>
                <p>{{$intro->text}}</p>
                <a class="cta-btn" href="#contact">Biz bilan aloqa</a>
            </div>
        </section><!-- End Call To Action Section -->

        <!-- ======= Our Services Section ======= -->
        <section id="services" class="services sections-bg">
            <div class="container" data-aos="fade-up">

                <div class="section-header">
                    <h2>Bizning Xizmatlar</h2>
                </div>

                <div class="row gy-4" data-aos="fade-up" data-aos-delay="100">

                   @foreach($service_card as $services)
                        <div class="col-lg-4 col-md-6">
                            <div class="service-item  position-relative">
                                <div class="icon">
                                    <img width="100px" height="100px" src="{{asset('public/storage/images/'.$services->image)}}">
                                </div>
                                <h3>{{$services->service_name}}</h3>
                                <p>{{$services->service_text}}</p>
                                <a href="{{$services->more}}" class="readmore stretched-link">Ko'proq o'qish <i class="bi bi-arrow-right"></i></a>
                            </div>
                        </div><!-- End Service Item -->
                       @endforeach

                </div>

            </div>
        </section><!-- End Our Services Section -->

        <!-- ======= Testimonials Section ======= -->
        <section id="testimonials" class="testimonials">
            <div class="container" data-aos="fade-up">

                <div class="section-header">
                    <h2>Komentariyalar</h2>
                </div>

                <div class="slides-3 swiper" data-aos="fade-up" data-aos-delay="100">
                    <div class="swiper-wrapper">
                        @foreach($part as $participanta)
                            <div class="swiper-slide">
                                <div class="testimonial-wrap">
                                    <div class="testimonial-item">
                                        <div class="d-flex align-items-center">
                                            <img width="100px" height="100px"  src="{{asset('public/storage/images/'.$participanta->card_image)}}" class="rounded-pill" alt="{{$participanta->card_image}}">
                                            <div>
                                                <h3 class="mx-2">{{$participanta->card_name}}</h3>
                                                <h4 class="mx-2">{{$participanta->job}}</h4>
                                                <div class="stars">
                                                    <i class="bi bi-star-fill"></i><i class="bi bi-star-fill"></i><i class="bi bi-star-fill"></i><i class="bi bi-star-fill"></i><i class="bi bi-star-fill"></i>
                                                </div>
                                            </div>
                                        </div>
                                        <p>
                                            <i class="bi bi-quote quote-icon-left"></i>
                                            {{$participanta->card_text}}
                                            <i class="bi bi-quote quote-icon-right"></i>
                                        </p>
                                    </div>
                                </div>
                            </div><!-- End testimonial item -->

                        @endforeach

                    </div>
                    <div class="swiper-pagination"></div>
                </div>

            </div>
        </section><!-- End Testimonials Section -->

        <!-- ======= Portfolio Section ======= -->
        <section id="portfolio" class="portfolio sections-bg">
            <div class="container" data-aos="fade-up">

                <div class="section-header">
                    <h2>Fanlar</h2>
                    <p>Bu yerdan o'zingizga kerakli fanlar haqida ma'lumotlarni olishingiz mumkin</p>
                </div>

                <div class="portfolio-isotope" data-portfolio-filter="*" data-portfolio-layout="masonry" data-portfolio-sort="original-order" data-aos="fade-up" data-aos-delay="100">

                    <div>
                        <ul class="portfolio-flters">
                            <li data-filter="*" class="filter-active">Barchasi</li>
                            <li data-filter=".aniq_fanlar">Aniq fanlar</li>
                            <li data-filter=".tabiiy_fanlar">Tabiiy fanlar</li>
                            <li data-filter=".tillar">Tillar</li>
                        </ul><!-- End Portfolio Filters -->
                    </div>

                    <div class="row gy-4 portfolio-container">


                    @foreach($portfolio as $item)

                            <div class="col-xl-4 col-md-6 portfolio-item {{$item->category}}">
                                <div class="portfolio-wrap">
                                    <a href="{{asset('public/storage/images/'.$item->image)}}" data-gallery="portfolio-gallery-app" class="glightbox"><img style="width: 550px; height: 300px" src="{{asset('public/storage/images/'.$item->image)}}" class="img-fluid" alt="{{$item->image}}"></a>
                                    <div class="portfolio-info">
                                        <h4>{{$item->name}}</h4>
                                        <p>{{$item->text}}</p>
                                    </div>
                                </div>
                            </div><!-- End Portfolio Item -->

                        @endforeach

                    </div><!-- End Portfolio Container -->

                </div>

            </div>
        </section><!-- End Portfolio Section -->

        <!-- ======= Our Team Section ======= -->
        <section id="team" class="team">
            <div class="container" data-aos="fade-up">

                <div class="section-header">
                    <h2>Bizning Ustozlar</h2>
                </div>

                <div class="row gy-4">

                   @foreach($teacher as $teachers)
                        <div class="col-xl-3 col-md-6 d-flex" data-aos="fade-up" data-aos-delay="100">
                            <div class="member">
                                <img src="{{asset('public/storage/images/'.$teachers->image)}}" width="300px" height="250px" class="" alt="{{$teachers->image}}">
                                <h4>{{$teachers->teacher_name}}</h4>
                                <span>{{$teachers->teacher_job}}</span>
                                <div class="social">
                                    <a href="{{$teachers->twitter}}"><i class="bi bi-twitter"></i></a>
                                    <a href="{{$teachers->facebook_icon}}"><i class="bi bi-facebook"></i></a>
                                    <a href="{{$teachers->instagram_icon}}"><i class="bi bi-instagram"></i></a>
                                    <a href="{{$teachers->telegram_icon}}"><i class="bi bi-telegram"></i></a>
                                </div>
                            </div>
                        </div><!-- End Team Member -->
                    @endforeach

                </div>

            </div>
        </section><!-- End Our Team Section -->

        <!-- ======= Pricing Section ======= -->
        <section id="pricing" class="pricing sections-bg">
            <div class="container" data-aos="fade-up">

                <div class="section-header">
                    <h2>Narxlar</h2>
                </div>

                <div class="row g-4 py-lg-5" data-aos="zoom-out" data-aos-delay="100">

                    <div class="col-lg-4">
                        <div class="pricing-item">
                            <h3>{{$pricing[0]->name}}</h3>
                            <div class="icon">
                                <i class="bi bi-box"></i>
                            </div>
                            <h4><sup>Uzs</sup>{{$pricing[0]->price_number}}<span> / oyiga</span></h4>
                            <ul>
                                <li><i class="bi bi-check"></i> {{$pricing[0]->correct_text}}</li>
                                <li class="na"><i class="bi bi-x"></i> <span>{{$pricing[0]->incorrect_text}}</span></li>
                                <li><i class=""></i><div class="text-center"><a href="#contact" class="buy-btn">Hoziroq aloqaga chiqing</a></div>
                                </li>

                                </li>
                            </ul>
                        </div>
                    </div><!-- End Pricing Item -->

                    <div class="col-lg-4">
                        <div class="pricing-item featured">
                            <h3>{{$pricing[1]->name}}</h3>
                            <div class="icon">
                                <i class="bi bi-airplane"></i>
                            </div>

                            <h4><sup>Uzs</sup>{{$pricing[1]->price_number}}<span> / oyiga</span></h4>
                            <ul>
                                <li><i class="bi bi-check"></i> {{$pricing[1]->correct_text}}</li>
                                <li class="na"><i class="bi bi-x"></i> <span>{{$pricing[1]->incorrect_text}}</span></li>

                                <li><i class=""></i><div class="text-center"><a href="#contact" class="buy-btn">Hoziroq aloqaga chiqing</a></div>
                                </li>

                            </ul>
                        </div>
                    </div><!-- End Pricing Item -->

                    <div class="col-lg-4">
                        <div class="pricing-item">
                            <h3>{{$pricing[2]->name}}</h3>
                            <div class="icon">
                                <i class="bi bi-send"></i>
                            </div>
                            <h4><sup>Uzs</sup>{{$pricing[2]->price_number}}<span> / oyiga</span></h4>
                            <ul>
                                <li><i class="bi bi-check"></i> {{$pricing[2]->correct_text}}</li>
                                <li class="na"><i class="bi bi-x"></i> <span>{{$pricing[2]->incorrect_text}}</span></li>
                                <li><i class=""></i><div class="text-center"><a href="#contact" class="buy-btn">Hoziroq aloqaga chiqing</a></div>
                                </li>

                                </li>
                            </ul>
                        </div>
                    </div><!-- End Pricing Item -->

                </div>

            </div>
        </section><!-- End Pricing Section -->

        <!-- ======= Frequently Asked Questions Section ======= -->


        <!-- ======= Contact Section ======= -->
        <section id="contact" class="contact">
            <div class="container" data-aos="fade-up">

                <div class="section-header">
                    <h2>Bog'lanish</h2>
                </div>

                <div class="row gx-lg-0 gy-4 d-flex justify-content-between">

                    <div class="col-lg-4">

                        <div class="info-container d-flex flex-column align-items-center justify-content-center">
                            <div class="info-item d-flex">
                                <i class="bi bi-geo-alt flex-shrink-0"></i>
                                <div>
                                    <h4>Joylashuv:</h4>
                                    <p>{{$message->location}}</p>
                                </div>
                            </div><!-- End Info Item -->

                            <div class="info-item d-flex">
                                <i class="bi bi-envelope flex-shrink-0"></i>
                                <div>
                                    <h4>Email:</h4>
                                    <p>{{$message->email}}</p>
                                </div>
                            </div><!-- End Info Item -->

                            <div class="info-item d-flex">
                                <i class="bi bi-phone flex-shrink-0"></i>
                                <div>
                                    <h4>Bog'lanish uchun:</h4>
                                    <p>{{$message->call_number}}</p>
                                </div>
                            </div><!-- End Info Item -->

                            <div class="info-item d-flex">
                                <i class="bi bi-clock flex-shrink-0"></i>
                                <div>
                                    <h4>Ish vaqti:</h4>
                                    <p>Du-Sha:{{$message->open_hours}}</p>
                                </div>
                            </div><!-- End Info Item -->
                        </div>

                    </div>

                    <div class="col-lg-7  " >
                        <form action="{{route('sendmessage.store')}}" method="post">
                            @csrf
                            @method('POST')
                            <div class="row">
                                <div class="col-md-6 form-group">
                                    <input type="text" name="name" class="form-control " id="name" placeholder="To'liq ismingiz" required>
                                </div>
                                <div class="col-md-6 form-group mt-3 mt-md-0">
                                    <input type="number" class="form-control" name="number" id="number" placeholder="Telefin raqamingiz" required>
                                </div>
                            </div>
                            <div class="form-group mt-3">
                                <input type="text" class="form-control" name="subject" id="subject" placeholder="Xabarning mavzusi" required>
                            </div>
                            <div class="form-group mt-3">
                                <textarea class="form-control" name="message" rows="7" placeholder="Xabar" required></textarea>
                            </div>
                            <div class="text-center my-5"><button class="btn btn-outline-success" type="submit">Xabarni yuborish</button></div>
                            @if (\Session::has('success'))
                                <div class="alert alert-success">
                                    <ul>
                                        <li>{!! \Session::get('success') !!}</li>
                                    </ul>
                                </div>
                            @endif
                        </form>
                    </div><!-- End Contact Form -->

                </div>

            </div>
        </section><!-- End Contact Section -->

    </main><!-- End #main -->

    <!-- ======= Footer ======= -->
    <footer id="footer" class="footer">

        <div class="container">
            <div class="row gy-4">
                <div class="text-center">
                        <h2>
                            Samo bilan samolarga parvoz qiling!!
                        </h2>

                </div>



            </div>
        </div>

        <div class="container mt-4">
            <div class="copyright">
                &copy; Creaded by Omadullo && Xabibullo
            </div>
            <div class="credits">
                <!-- All the links in the footer should remain intact. -->
                <!-- You can delete the links only if you purchased the pro version. -->
                <!-- Licensing information: https://bootstrapmade.com/license/ -->
                <!-- Purchase the pro version with working PHP/AJAX contact form: https://bootstrapmade.com/impact-bootstrap-business-website-template/ -->
            </div>
        </div>

    </footer><!-- End Footer -->
    <!-- End Footer -->

    <a href="#" class="scroll-top d-flex align-items-center justify-content-center"><i class="bi bi-arrow-up-short"></i></a>

    <div id="preloader"></div>


@endsection
