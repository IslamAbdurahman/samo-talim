<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta content="width=device-width, initial-scale=1.0" name="viewport">

    <title>SAMO TA'LIM</title>
    <meta content="" name="description">
    <meta content="" name="keywords">

    <!-- Favicons -->
    <link  class="rounded-pill" href="public/samo/samo_photo.jpg" rel="icon">
    <link href="public/samo/assets/img/apple-touch-icon.png" rel="apple-touch-icon">

    <!-- Google Fonts -->
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Open+Sans:ital,wght@0,300;0,400;0,500;0,600;0,700;1,300;1,400;1,600;1,700&family=Montserrat:ital,wght@0,300;0,400;0,500;0,600;0,700;1,300;1,400;1,500;1,600;1,700&family=Raleway:ital,wght@0,300;0,400;0,500;0,600;0,700;1,300;1,400;1,500;1,600;1,700&display=swap" rel="stylesheet">

    <!-- Vendor CSS Files -->
    <link href="public/samo/assets/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link href="public/samo/assets/vendor/bootstrap-icons/bootstrap-icons.css" rel="stylesheet">
    <link href="public/samo/assets/vendor/aos/aos.css" rel="stylesheet">
    <link href="public/samo/assets/vendor/glightbox/css/glightbox.min.css" rel="stylesheet">
    <link href="public/samo/assets/vendor/swiper/swiper-bundle.min.css" rel="stylesheet">

    <!-- Template Main CSS File -->
    <link href="public/samo/assets/css/main.css" rel="stylesheet">

    <!-- =======================================================
    * Template Name: Impact - v1.1.1
    * Template URL: https://bootstrapmade.com/impact-bootstrap-business-website-template/
    * Author: BootstrapMade.com
    * License: https://bootstrapmade.com/license/
    ======================================================== -->
</head>

<body>




@yield('main')





<!-- Vendor JS Files -->
<script src="public/samo/assets/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
<script src="public/samo/assets/vendor/aos/aos.js"></script>
<script src="public/samo/assets/vendor/glightbox/js/glightbox.min.js"></script>
<script src="public/samo/assets/vendor/purecounter/purecounter_vanilla.js"></script>
<script src="public/samo/assets/vendor/swiper/swiper-bundle.min.js"></script>
<script src="public/samo/assets/vendor/isotope-layout/isotope.pkgd.min.js"></script>
<script src="public/samo/assets/vendor/php-email-form/validate.js"></script>

<!-- Template Main JS File -->
<script src="public/samo/assets/js/main.js"></script>
</body>

</html>
