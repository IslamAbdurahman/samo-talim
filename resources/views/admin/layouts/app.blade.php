<!doctype html>
<html lang="en">

<head>

    <meta charset="utf-8" />
    <title>SAMO TA'LIM ADMIN PANEL</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta content="Premium Multipurpose Admin & Dashboard Template" name="description" />
    <meta content="Themesbrand" name="author" />
    <!-- App favicon -->
    <link rel="stylesheet" href="public">
    <link rel="shortcut icon" href="public/samo/admin/assets/images/favicon.ico">
    <link href="public/samo/assets/vendor/bootstrap-icons/bootstrap-icons.css" rel="stylesheet">

    <!-- Bootstrap Css -->
    <link href="public/samo/admin/assets/css/bootstrap.min.css" id="bootstrap-style" rel="stylesheet" type="text/css" />
    <!-- Icons Css -->
    <link href="public/samo/admin/assets/css/icons.min.css" rel="stylesheet" type="text/css" />
    <!-- App Css-->
    <link href="public/samo/admin/assets/css/app.min.css" id="app-style" rel="stylesheet" type="text/css" />

</head>

<body data-sidebar="dark">


<!-- Begin page -->
<div id="layout-wrapper">


    <header id="page-topbar">
        <div class="navbar-header">
            <div class="d-flex">
                <!-- LOGO -->
                <div class="navbar-brand-box">
                    <a href="index.html" class="logo logo-dark">
                                <span class="logo-sm" height="22">
{{--                                    <img src="public/samo/admin/assets/images/logo.svg" alt="" height="22">--}}
                                   Admin
                                </span>
                        <span class="logo-lg" height="17">
{{--                                    <img src="public/samo/admin/assets/images/logo-dark.png" alt="" height="17">--}}
                                </span>
                    </a>

                    <a href="#" class="logo logo-light">
                                <span class="logo-sm">
                                    <img src="public/samo/admin/assets/images/logo-light.svg" alt="" height="22">
                                </span>
                        <span class="logo-lg">
                                    <img src="public/samo/admin/assets/images/logo-light.png" alt="" height="19">
                                </span>
                    </a>
                </div>

                <button type="button" class="btn btn-sm px-3 font-size-16 header-item waves-effect" id="vertical-menu-btn">
                    <i class="fa fa-fw fa-bars"></i>
                </button>


            </div>

            <div class="d-flex">

                <div class="dropdown d-inline-block d-lg-none ms-2">
                    <button type="button" class="btn header-item noti-icon waves-effect" id="page-header-search-dropdown"
                            data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <i class="mdi mdi-magnify"></i>
                    </button>
                    <div class="dropdown-menu dropdown-menu-lg dropdown-menu-end p-0"
                         aria-labelledby="page-header-search-dropdown">

                        <form class="p-3">
                            <div class="form-group m-0">
                                <div class="input-group">
                                    <input type="text" class="form-control" placeholder="Search ..." aria-label="Recipient's username">
                                    <div class="input-group-append">
                                        <button class="btn btn-primary" type="submit"><i class="mdi mdi-magnify"></i></button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>

                <div class="dropdown d-inline-block">
                    <button type="button" class="btn header-item waves-effect"
                            data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <img id="header-lang-img" src="public/samo/admin/assets/images/flags/uzb_flag.jfif" alt="Bayroqlar" height="16">
                    </button>

                </div>



                <div class="dropdown d-none d-lg-inline-block ms-1">
                    <button type="button" class="btn header-item noti-icon waves-effect" data-bs-toggle="fullscreen">
                        <i class="bx bx-fullscreen"></i>
                    </button>
                </div>



                <div class="dropdown d-inline-block">
                    <button type="button" class="btn header-item waves-effect" id="page-header-user-dropdown"
                            data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <img class="rounded-circle header-profile-user" width="50px" height="50px" src="public/samo/samo_photo.jpg"
                             alt="Header Avatar">
                        <span class="d-none d-xl-inline-block ms-1" key="t-henry">Samo Admin</span>
                        <i class="mdi mdi-chevron-down d-none d-xl-inline-block"></i>
                    </button>
                    <div class="dropdown-menu dropdown-menu-end">
                        <!-- item-->

                            <div class="dropdown-menu dropdown-menu-end d-inline" aria-labelledby="navbarDropdown">
                                <a class="dropdown-item" href="{{ route('logout') }}"
                                   onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                    <i class="bx bx-power-off font-size-16 align-middle me-1 text-danger"></i>
                                    <span key="t-logout">Logout</span>

                                </a>

                                <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                                    @csrf
                                </form>
                            </div>


                        </a>
                    </div>
                </div>

                <div class="dropdown d-inline-block">
                    <button type="button" class="btn header-item noti-icon right-bar-toggle waves-effect">
                        <i class="bx bx-cog bx-spin"></i>
                    </button>
                </div>

            </div>
        </div>
    </header>

    <!-- ========== Left Sidebar Start ========== -->
    <div class="vertical-menu">

        <div data-simplebar class="h-100">

            <!--- Sidemenu -->
            <div id="sidebar-menu">
                <!-- Left Menu Start -->
                <ul class="metismenu list-unstyled" id="side-menu">

                    <li class="text-center">
                        <a href="{{route('admin_data.index')}}"><img class="rounded-pill " width="100px"  src="/public/samo/samo_photo.jpg"></a>
                    </li>



                    <li class="menu-title" key="t-apps">Bo'limlar</li>



                    <li>
                        <a href="{{route('about_left.index')}}" class="waves-effect">
                            <i class="bx bx-home-circle"></i><span class="badge rounded-pill bg-info float-end"></span>
                            <span key="t-dashboards">Biz Haqimizda</span>
                        </a>
                    </li>


                    <li>
                        <a href="{{route('cards.index')}}" class="waves-effect">
                            <i class="bx bx-home-circle"></i><span class="badge rounded-pill bg-info float-end"></span>
                            <span key="t-dashboards">Kardlar</span>
                        </a>
                    </li>
                    <li>
                        <a href="{{route('home_main.index')}}" class="waves-effect">
                            <i class="bx bx-home-circle"></i><span class="badge rounded-pill bg-info float-end"></span>
                            <span key="t-dashboards">Asosiy ko'rinish</span>
                        </a>
                    </li>
                    <li>
                        <a href="{{route('introduction.index')}}" class="waves-effect">
                            <i class="bx bx-home-circle"></i><span class="badge rounded-pill bg-info float-end"></span>
                            <span key="t-dashboards">Tanishtiruv</span>
                        </a>
                    </li>
                    <li>
                        <a href="{{route('message.index')}}" class="waves-effect">
                            <i class="bx bx-home-circle"></i><span class="badge rounded-pill bg-info float-end"></span>
                            <span key="t-dashboards">Bog'lanish malumotlari</span>
                        </a>
                    </li>

                    <li>
                        <a href="{{route('participants.index')}}" class="waves-effect">
                            <i class="bx bx-home-circle"></i><span class="badge rounded-pill bg-info float-end"></span>
                            <span key="t-dashboards">Komentariya</span>
                        </a>
                    </li>
                    <li>
                        <a href="{{route('portfolio.index')}}" class="waves-effect">
                            <i class="bx bx-home-circle"></i><span class="badge rounded-pill bg-info float-end"></span>
                            <span key="t-dashboards">Fanlar</span>
                        </a>
                    </li>
                    <li>
                        <a href="{{route('pricing.index')}}" class="waves-effect">
                            <i class="bx bx-home-circle"></i><span class="badge rounded-pill bg-info float-end"></span>
                            <span key="t-dashboards">Narxlar</span>
                        </a>
                    </li>
                    <li>
                        <a href="{{route('service_card.index')}}" class="waves-effect">
                            <i class="bx bx-home-circle"></i><span class="badge rounded-pill bg-info float-end"></span>
                            <span key="t-dashboards">Xizmatlarimiz </span>
                        </a>
                    </li>
                    <li>
                        <a href="{{route('teachers.index')}}" class="waves-effect">
                            <i class="bx bx-home-circle"></i><span class="badge rounded-pill bg-info float-end"></span>
                            <span key="t-dashboards">O'qituvchilar</span>
                        </a>
                    </li>








                </ul>
                <ul>
                </ul>
            </div>
            <!-- Sidebar -->
        </div>
    </div>
    <!-- Left Sidebar End -->



    <!-- ============================================================== -->
    <!-- Start right Content here -->
    <!-- ============================================================== -->
    <div class="main-content">

        <div class="page-content">
            <div class="container-fluid">

            @yield('main')


            <!-- container-fluid -->
        </div>


        <footer class="footer">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-sm-6">
                        <script>document.write(new Date().getFullYear())</script> © XABIBULLO & OMADULLO.
                    </div>
                    <div class="col-sm-6">
                        <div class="text-sm-end d-none d-sm-block">
                            Design & Develop XABIBULLO & OMADULLO
                        </div>
                    </div>
                </div>
            </div>
        </footer>
    </div>
    <!-- end main content-->

</div>
<!-- END layout-wrapper -->

{{--    <div class="right-bar">--}}
{{--        <div data-simplebar class="h-100">--}}
{{--            <div class="rightbar-title d-flex align-items-center px-3 py-4">--}}

{{--                <h5 class="m-0 me-2">Settings</h5>--}}

{{--                <a href="javascript:void(0);" class="right-bar-toggle ms-auto">--}}
{{--                    <i class="mdi mdi-close noti-icon"></i>--}}
{{--                </a>--}}
{{--            </div>--}}

{{--            <!-- Settings -->--}}
{{--            <hr class="mt-0" />--}}
{{--            <h6 class="text-center mb-0">Choose Layouts</h6>--}}

{{--            <div class="p-4">--}}
{{--                <div class="mb-2">--}}
{{--                    <img src="public/samo/admin/assets/images/layouts/layout-1.jpg" class="img-thumbnail" alt="layout images">--}}
{{--                </div>--}}

{{--                <div class="form-check form-switch mb-3">--}}
{{--                    <input class="form-check-input theme-choice" type="checkbox" id="light-mode-switch" checked>--}}
{{--                    <label class="form-check-label" for="light-mode-switch">Light Mode</label>--}}
{{--                </div>--}}

{{--                <div class="mb-2">--}}
{{--                    <img src="public/samo/admin/assets/images/layouts/layout-2.jpg" class="img-thumbnail" alt="layout images">--}}
{{--                </div>--}}
{{--                <div class="form-check form-switch mb-3">--}}
{{--                    <input class="form-check-input theme-choice" type="checkbox" id="dark-mode-switch">--}}
{{--                    <label class="form-check-label" for="dark-mode-switch">Dark Mode</label>--}}
{{--                </div>--}}

{{--                <div class="mb-2">--}}
{{--                    <img src="public/samo/admin/assets/images/layouts/layout-3.jpg" class="img-thumbnail" alt="layout images">--}}
{{--                </div>--}}
{{--                <div class="form-check form-switch mb-3">--}}
{{--                    <input class="form-check-input theme-choice" type="checkbox" id="rtl-mode-switch">--}}
{{--                    <label class="form-check-label" for="rtl-mode-switch">RTL Mode</label>--}}
{{--                </div>--}}

{{--                <div class="mb-2">--}}
{{--                    <img src="public/samo/admin/assets/images/layouts/layout-4.jpg" class="img-thumbnail" alt="layout images">--}}
{{--                </div>--}}
{{--                <div class="form-check form-switch mb-5">--}}
{{--                    <input class="form-check-input theme-choice" type="checkbox" id="dark-rtl-mode-switch">--}}
{{--                    <label class="form-check-label" for="dark-rtl-mode-switch">Dark RTL Mode</label>--}}
{{--                </div>--}}


{{--            </div>--}}

{{--        </div> <!-- end slimscroll-menu-->--}}
{{--    </div>--}}








<!-- JAVASCRIPT -->
<script src="{{asset('public/samo/admin/assets/libs/jquery/jquery.min.js')}}"></script>
<script src="{{asset('public/samo/admin/assets/libs/bootstrap/js/bootstrap.bundle.min.js')}}"></script>
<script src="{{asset('public/samo/admin/assets/libs/metismenu/metisMenu.min.js')}}"></script>
<script src="{{asset('public/samo/admin/assets/libs/simplebar/simplebar.min.js')}}"></script>
<script src="{{asset('public/samo/admin/assets/libs/node-waves/waves.min.js')}}"></script>

<!-- apexcharts -->
<script src="{{asset('public/samo/admin/assets/libs/apexcharts/apexcharts.min.js')}}"></script>

<!-- dashboard init -->
<script src="{{asset('public/samo/admin/assets/js/pages/dashboard.init.js')}}"></script>

<!-- App js -->
<script src="{{asset('public/samo/admin/assets/js/app.js')}}"></script>
</body>

</html>
