@extends('admin.layouts.app')



@section("main")

   <div class="container">

           <button type="button" class="btn btn-outline-info col-3 text-center center m-1" data-bs-toggle="modal" data-bs-target="#exampleModalCreate" data-bs-whatever="@getbootstrap">Qo'shish</button>

           <div class="modal fade" id="exampleModalCreate" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
               <div class="modal-dialog">
                   <div class="modal-content">
                       <div class="modal-header">
                           <h5 class="modal-title" id="exampleModalLabel">Ma'lumotlarni Qo'sish</h5>
                           <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                       </div>
                       <div class="modal-body ">
                           <form action="{{route('participants.store')}}" method="post" enctype="multipart/form-data">
                               @csrf
                               @method('post')
                               <div class="mb-3">
                                   <label for="card_image" class="col-form-label">Rasmni kiriting</label>
                                   <input name="card_image" type="file" class="form-control" >

                                   <label for="card_name" class="col-form-label">Sarlavhani kiriting</label>
                                   <input name="card_name" type="text" class="form-control" placeholder="Sarlavha">

                                   <label for="job" class="col-form-label">Kasbni kiriting</label>
                                   <input name="job" type="text" class="form-control" placeholder="Kasb">

                                   <label for="card_text" class="col-form-label">Textni kiriting</label>
                                   <input name="card_text" type="text" class="form-control" placeholder="Text"">
                               </div>
                               <div class="modal-footer">
                                   <button  class="btn btn-secondary" data-bs-dismiss="modal">Bekor qilish</button>
                                   <button type="submit" class="btn btn-primary">Yaratish</button>
                               </div>
                           </form>
                       </div>

                   </div>
               </div>
           </div>



                       <div class="container">
                           <div class="row">
                               @foreach($participants as $item)

                               <div class="col-3">
                                   <div class="card">
                                       <div class="bg-image hover-overlay hover-zoom hover-shadow ripple">
                                           <img class="card-img-top w-100" src="{{ asset('public/storage/images/'.$item->card_image)  }}" alt="{{ $item->card_image }}" />
                                           <a href="#!">
                                               <div class="mask" style="background-color: rgba(57, 192, 237, 0.2)"></div>
                                           </a>
                                       </div>

                                       <div class="card-body">
                                           <h5 class="card-title">{{$item->card_name}}</h5>
                                           <p class="card-text">{{$item->job}}</p>
                                           <p class="card-text">{{$item->card_text}}</p>

                                           <button type="button" class="btn btn-primary" data-bs-toggle="modal" data-bs-target="#update{{$item->id}}" data-bs-whatever="@getbootstrap">O'zgartirish</button>

                                           <div class="modal fade" id="update{{$item->id}}" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                               <div class="modal-dialog">
                                                   <div class="modal-content">
                                                       <div class="modal-header">
                                                           <h5 class="modal-title" id="exampleModalLabel{{$item->id}}">Ma'lumotlarni Tahrirlash</h5>
                                                           <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                                                       </div>
                                                       <div class="modal-body">
                                                           <form action="{{route('participants.update',$item->id)}}" enctype="multipart/form-data" method="post">
                                                               @csrf
                                                               @method('put')

                                                               <div class="mb-3">
                                                                   <label for="card_image" class="col-form-label">Rasmni o'zgartirish</label>
                                                                   <input name="card_image" type="file"  class="form-control" value="{{$item->card_image}}" >

                                                                   <label for="card_name" class="col-form-label">Sarlavhani o'zgartirish</label>
                                                                   <input name="card_name" type="text" required class="form-control" value="{{ $item->card_name }}">

                                                                   <label for="job" class="col-form-label"> Kasbni o'zgartirish</label>
                                                                   <input name="job" type="text" required class="form-control" value="{{ $item->job }}" >

                                                                   <label for="card_text" class="col-form-label"> Textni o'zgartirish</label>
                                                                   <input name="card_text" type="text" required class="form-control" value="{{$item->card_text}}">
                                                               </div>
                                                               <div class="modal-footer">
                                                                   <button  class="btn btn-secondary" data-bs-dismiss="modal">Bekor qilish</button>
                                                                   <button type="submit" class="btn btn-primary">O'zgartirish</button>
                                                               </div>
                                                           </form>
                                                       </div>

                                                   </div>
                                               </div>
                                           </div>


                                           <button type="button" class="btn btn-outline-danger" data-bs-toggle="modal" data-bs-target="#exampleModal{{$item->id+1}}" data-bs-whatever="@getbootstrap">O'chirish</button>

                                           <div class="modal fade" id="exampleModal{{$item->id+1}}" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                               <div class="modal-dialog">
                                                   <div class="modal-content">
                                                       <div class="modal-header">
                                                           <h5 class="modal-title" id="exampleModalLabel{{$item->id+1}}">Ma'lumotlarni O'chirish</h5>
                                                           <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                                                       </div>
                                                       <div class="modal-body">
                                                           <form action="{{route('participants.destroy',$item->id)}}" enctype="multipart/form-data" method="post">
                                                               @csrf
                                                               @method('DELETE')
                                                               <div class="modal-body">
                                                                   <h3>Ishonchingiz komilmi?</h3>
                                                               </div>
                                                               <div class="modal-footer">
                                                                   <button  class="btn btn-secondary " type="button"  data-bs-dismiss="modal" aria-label="Close">Bekor qilish</button>
                                                                   <button type="submit" class="btn btn-danger">O'chirish</button>
                                                               </div>
                                                           </form>
                                                       </div>

                                                   </div>
                                               </div>
                                           </div>
                                       </div>

                                   </div>
                               </div>

                               @endforeach

                           </div>
                       </div>





   </div>





@endsection
