@extends('admin.layouts.app')


@section('main')



    <div class="container">
        <button type="button" class="btn btn-outline-info col-3 text-center center m-1" data-bs-toggle="modal" data-bs-target="#exampleModal" data-bs-whatever="@getbootstrap">Qo'shish</button>

        <div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Qo'shish</h5>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <div class="modal-body ">
                        <form action="{{route('service_card.store')}}" method="post" enctype="multipart/form-data">
                            @csrf
                            @method('post')
                            <div class="mb-3">
                                <label for="image" class="col-form-label">Rasmni kiriting</label>
                                <input name="image" type="file" class="form-control" >

                                <label for="service_name" class="col-form-label">Servisning nomni kiriting</label>
                                <input name="service_name" type="text" class="form-control" required placeholder="nom">

                                <label for="service_text" class="col-form-label"> Servisning tarifini kiriting</label>
                                <input name="service_text" type="text" class="form-control" required placeholder="text">

                                <label for="service_text" class="col-form-label">Kartni link manzilini o'zgartirish</label>
                                <input name="more" type="text" required class="form-control" placeholder="link">




                            </div>
                            <div class="modal-footer">
                                <button  class="btn btn-secondary" data-bs-dismiss="modal">Bekor qilish</button>
                                <button type="submit" class="btn btn-primary">Yaratish</button>
                            </div>
                        </form>
                    </div>

                </div>
            </div>
        </div>
        <div class="row justify-content-around">

            @foreach($service_card as $item)

                <div class="modal fade" id="exampleModalupdate{{$item->id}}" tabindex="-1" aria-labelledby="exampleModalLabelupdate{{$item->id}}" aria-hidden="true">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title" id="exampleModalLabelupdate{{$item->id}}">Yangi o'zgarish</h5>
                                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                            </div>
                            <div class="modal-body">
                                <form action="{{route('service_card.update',$item->id)}}" enctype="multipart/form-data" method="post">
                                    @csrf
                                    @method('put')

                                    <div class="mb-3">
                                        <label for="image" class="col-form-label">Rasmni o'zgartirish</label>
                                        <input name="image" type="file"  class="form-control" value="{{$item->image}}" >

                                        <label for="service_name" class="col-form-label">Servisning nomni o'zgartirish</label>
                                        <input name="service_name" type="text" required class="form-control" value="{{ $item->service_name }}">

                                        <label for="service_text" class="col-form-label">Servisning tarifini o'zgartirish</label>
                                        <input name="service_text" type="text" required class="form-control" value="{{ $item->service_text }}">

                                        <label for="service_text" class="col-form-label">Kartni link manzilini o'zgartirish</label>
                                        <input name="more" type="text" required class="form-control" value="{{ $item->more }}">



                                    </div>
                                    <div class="modal-footer">
                                        <button  class="btn btn-secondary" data-bs-dismiss="modal">Bekor qilish</button>
                                        <button type="submit" class="btn btn-primary">O'zgartirish</button>
                                    </div>
                                </form>
                            </div>

                        </div>
                    </div>
                </div>

                <div class="card col-4">
                    <img class="card-img-top" width="80px" height="80px" src="{{ asset('public/storage/images/'.$item->image)}}" alt="{{$item->image}}">
                    <div class="card-body">
                        <h2 class="card-text">{{$item->service_name}}</h2>
                        <p class="card-text">{{$item->service_text}}</p>

                        <button type="button" class="btn btn-primary" data-bs-toggle="modal" data-bs-target="#exampleModalupdate{{$item->id}}" data-bs-whatever="@getbootstrap">O'zgartirish</button>



                        <button type="button" class="btn btn-outline-danger" data-bs-toggle="modal" data-bs-target="#exampleModaldelete{{$item->id}}" data-bs-whatever="@getbootstrap">O'chirish</button>

                        <div class="modal fade" id="exampleModaldelete{{$item->id}}" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h5 class="modal-title" id="exampleModalLabel{{$item->id+1}}">O'chirish</h5>
                                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                                    </div>
                                    <div class="modal-body">
                                        <form action="{{route('service_card.destroy',$item->id)}}" enctype="multipart/form-data" method="post">
                                            @csrf
                                            @method('DELETE')
                                            <div class="modal-body">
                                                <h3>Ishonchingiz komilmi?</h3>
                                            </div>
                                            <div class="modal-footer">
                                                <button  class="btn btn-secondary " type="button"  data-bs-dismiss="modal" aria-label="Close">Bekor qilish</button>

                                                <button type="submit" class="btn btn-danger">O'chirish</button>
                                            </div>
                                        </form>
                                    </div>

                                </div>
                            </div>
                        </div>

                    </div>
                </div>


            @endforeach
        </div>

    </div>


@endsection
