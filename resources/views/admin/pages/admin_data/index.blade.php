
        <!doctype html>
<html lang="en">

<head>

    <meta charset="utf-8" />
    <title>Recover Password | Skote - Admin & Dashboard Template</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta content="Premium Multipurpose Admin & Dashboard Template" name="description" />
    <meta content="Themesbrand" name="author" />
    <!-- App favicon -->
    <link rel="shortcut icon" href="public/samo/admin/assets/images/favicon.ico">

    <!-- Bootstrap Css -->
    <link href="public/samo/admin/assets/css/bootstrap.min.css" id="bootstrap-style" rel="stylesheet" type="text/css" />
    <!-- Icons Css -->
    <link href="public/samo/admin/assets/css/icons.min.css" rel="stylesheet" type="text/css" />
    <!-- App Css-->
    <link href="public/samo/admin/assets/css/app.min.css" id="app-style" rel="stylesheet" type="text/css" />

</head>

<body>
<div class="account-pages my-5 pt-sm-5">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8 col-lg-6 col-xl-5">
                <div class="card overflow-hidden">
                    <div class="bg-primary bg-soft">
                        <div class="row">
                            <div class="col-7">
                                <div class="text-primary p-4">
                                    <h5 class="text-primary"> Ma'lumotlarni yangilash</h5>
                                    <p>Reset Password with Skote.</p>
                                </div>
                            </div>
                            <div class="col-5 align-self-end">
                                <img src="public/samo/admin/assets/images/profile-img.png" alt="" class="img-fluid">
                            </div>
                        </div>
                    </div>
                    <div class="card-body pt-0">
                        <div>
                            <a href="index.html">
                                <div class="avatar-md profile-user-wid mb-4">
                                            <span class="avatar-title rounded-circle bg-light">
                                                <img src="public/samo/admin/assets/images/logo.svg" alt="" class="rounded-circle" height="34">
                                            </span>
                                </div>
                            </a>
                        </div>

                        <div class="p-2">
                            <div class="alert alert-success text-center mb-4" role="alert">
                                Ma'lumotlaringizni yangilash uchun quyidagi ma'lumotlarni kirgazing
                            </div>
                            <form class="form-horizontal" action="{{route('admin_data.store')}}" method="post">
                                    @csrf
                                @method('post')
                                <div class="mb-3">
                                    <label for="useremail" class="form-label">Email</label>
                                    <input type="email" class="form-control" name="email" id="useremail" placeholder=" email"  required>
                                </div>
                                <div class="mb-3">
                                    <label for="newpass" class="form-label">Iltimos yangi parolni kirgazing</label>
                                    <input type="text" class="form-control" name="password" id="newpass" placeholder="Yangi parol" required>
                                    <div>
                                        <p class="text-danger">Talablar::</p>
                                        <span class="text-success">Eng kamida 8 ta ma'lumotdan foydalaning</span> <br>
                                        <span class="text-success">Xariflardan foydalaning</span> <br>
                                        <span class="text-success">Katta va kichich xariflardan foydalaning</span> <br>
                                        <span class="text-success">Maxsus belgilardan foydalaning</span><br>

                                    </div>
                                </div>
                                <div class="mb-3">
                                    <label for="verifypass" class="form-label"> Parolni tasqiqlang</label>
                                    <input type="text" class="form-control" name="verifypass" id="verifypass" placeholder="Tasdiqlang" required>
                                </div>

                                <div class="text-center">
                                    <button class="btn btn-primary w-md waves-effect waves-light" type="submit">Yangilash</button>
                                </div>

                            </form>
                            @if (\Session::has('success'))
                                <div class="alert alert-success">
                                    <ul>
                                        <li>{!! \Session::get('success') !!}</li>
                                    </ul>
                                </div>
                            @endif
                            @if (\Session::has('failure'))
                                <div class="alert alert-danger">
                                    <ul>
                                        <li>{!! \Session::get('failure') !!}</li>
                                    </ul>
                                </div>
                            @endif
                            @if (\Session::has('xato'))
                                <div class="alert alert-danger">
                                    <ul>
                                        <li>{!! \Session::get('xato') !!}</li>
                                    </ul>
                                </div>
                            @endif
                        </div>

                    </div>
                </div>
                <div class="mt-5 text-center">
                    <p>Parolni esladingizmi <a href="{{route('login')}}" class="fw-medium text-primary">Log in</a> </p>
                    <p>© <script>document.write(new Date().getFullYear())</script> Skote. Crafted with <i class="mdi mdi-heart text-danger"></i> by Themesbrand</p>
                </div>

            </div>
        </div>
    </div>
</div>

<!-- JAVASCRIPT -->
<script src="public/samo/admin/assets/libs/jquery/jquery.min.js"></script>
<script src="public/samo/admin/assets/libs/bootstrap/js/bootstrap.bundle.min.js"></script>
<script src="public/samo/admin/assets/libs/metismenu/metisMenu.min.js"></script>
<script src="public/samo/adminassets/libs/simplebar/simplebar.min.js"></script>
<script src="public/samo/admin/assets/libs/node-waves/waves.min.js"></script>

<!-- App js -->
<script src="public/samo/admin/assets/js/app.js"></script>
</body>
</html>

