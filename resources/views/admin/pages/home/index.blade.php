@extends('admin.layouts.app')


@section('main')
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
    <div>
        <div class="mb-5 mt-5">
            <h3 class="text-center">
                Bosh sahifani tahrirlash
            </h3>
        </div>
        <div style="background-color: #008374" class="container position-relative ">
            <div class="row gy-5" data-aos="fade-in">
                <div class="col-lg-6 order-2 order-lg-1 d-flex flex-column justify-content-center text-center text-lg-start">
                    <h2 class="text-white">{{$home->header_name}}</h2>
                    <p style="color: #82c2bb">{{$home->text}}</p>
                    <div class="d-flex justify-content-around col-lg-6 ">
                        <div class="">
                            <a href="#about" style="background-color: #008374; color: white; border-radius: 20px" class="btn btn-outline-light ">Get Started</a>
                        </div>
                       <div class="">
                           <a target="_blank" href="{{$home->video}}" style="color: #80c1ba" class="glightbox btn-watch-video d-flex align-items-center  "><svg   xmlns="http://www.w3.org/2000/svg" width="30" height="30" fill="currentColor" class="bi bi-play-circle " viewBox="0 0 16 16">
                                   <path d="M8 15A7 7 0 1 1 8 1a7 7 0 0 1 0 14zm0 1A8 8 0 1 0 8 0a8 8 0 0 0 0 16z"/>
                                   <path d="M6.271 5.055a.5.5 0 0 1 .52.038l3.5 2.5a.5.5 0 0 1 0 .814l-3.5 2.5A.5.5 0 0 1 6 10.5v-5a.5.5 0 0 1 .271-.445z"/>
                               </svg><span class="text-white px-3">Watch Video</span></a>
                       </div>
                    </div>
                </div>
                <div class="col-lg-6 order-1 order-lg-2">
                    <img src="{{ asset('public/storage/images/'.$home->photo)  }}" alt="{{ $home->photo }}" class="img-fluid" alt="" data-aos="zoom-out" data-aos-delay="100">
                </div>
            </div>
        </div>


       <div class="text-center py-4">
           <div class="">
               <button type="button" class="btn btn-primary" data-bs-toggle="modal" data-bs-target="#update" data-bs-whatever="@getbootstrap">Ma'lumotlarni Tahrirlash</button>
           </div>
           <div class="modal fade" id="update" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
               <div class="modal-dialog">
                   <div class="modal-content">
                       <div class="modal-header">
                           <h5 class="modal-title" id="exampleModalLabel">Bosh sahifa</h5>
                           <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                       </div>
                       <div class="modal-body">
                           <form action="{{route('home_main.update', $home->id)}}" method="post" enctype="multipart/form-data" >
                               @csrf
                               @method('put')
                               <div class="mb-3">
                                   <label for="email" class="form-label">Bosh yozuv</label>
                                   <input type="text" class="form-control" name="header_name" value="{{$home->header_name}}">

                                   <label for="email" class="form-label">Oddiy yozuv</label>
                                   <input type="text" class="form-control" name="text" value="{{$home->text}}">

                                   <label for="email" class="form-label">Surat</label>
                                   <input type="file" class="form-control" name="photo" value="{{$home->photo}}">

                                   <label for="email" class="form-label">Video Link</label>
                                   <input type="text" class="form-control" name="video" value="{{$home->video}}">
                               </div>
                               <div class="modal-footer">
                                   <button type="submit" class="btn btn-secondary" data-bs-dismiss="modal">Yopish</button>
                                   <button type="submit" class="btn btn-primary">O'zgarishlarni saqlash</button>
                               </div>
                           </form>
                       </div>

                   </div>
               </div>
           </div>
       </div>


    </div>

    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.9.2/dist/umd/popper.min.js" integrity="sha384-IQsoLXl5PILFhosVNubq5LC7Qb9DXgDA9i+tQ8Zj3iwWAwPtgFTxbJ8NT4GN1R8p" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.min.js" integrity="sha384-cVKIPhGWiC2Al4u+LWgxfKTRIcfu0JTxR+EQDz/bgldoEyl4H0zUF0QKbrJ0EcQF" crossorigin="anonymous"></script>
@endsection
