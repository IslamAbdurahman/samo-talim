@extends('admin.layouts.app')


@section('main')





    <button type="button" class="btn btn-outline-info col-3 text-center center m-1" data-bs-toggle="modal" data-bs-target="#exampleModal" data-bs-whatever="@getbootstrap">Qo'shish</button>

    <div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Qo'shish</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body ">
                    <form action="{{route('portfolio.store')}}" method="post" enctype="multipart/form-data">
                        @csrf
                        @method('post')
                        <div class="mb-3">
                            <label for="image" class="col-form-label">Rasmni kiriting</label>
                            <input name="image" type="file" class="form-control" >

                            <label for="name" class="col-form-label">Sarlavhani kiriting</label>
                            <input name="name" type="text" class="form-control" placeholder="Sarlavha">

                            <label for="text" class="col-form-label">Textni kiriting</label>
                            <input name="text" type="text" class="form-control" placeholder="text">

                            <label for="text" class="col-form-label">kategoriya tanlang</label>
                            <select name="cat_id" class="form-select">
                                @foreach($categories as $category)
                                    <option value="{{$category->id}}">

                                        {{$category->name}}

                                    </option>


                                @endforeach
                            </select>
                      


                        </div>
                        <div class="modal-footer">
                            <button  class="btn btn-secondary" data-bs-dismiss="modal">Bekor qilish</button>
                            <button type="submit" class="btn btn-primary">Yaratish</button>
                        </div>
                    </form>
                </div>

            </div>
        </div>
    </div>





    <div class="container">
       <div class="row  gy-4">


           @foreach($portfolio as $item)


               <div class="col-4 *"> <div class="card" style="width: 18rem;">
                       <img class="card-img-top" src="{{ asset('public/storage/images/'.$item->image)  }}" alt="{{ $item->image }}">
                       <div class="card-body">
                           <h1>{{$item->name}}</h1>
                           <p class="card-text">{{$item->text}}</p>
                            <p><i>{{$item->category}}</i></p>
                           <p>
                               <button type="button" class="btn btn-primary" data-bs-toggle="modal" data-bs-target="#exampleModal{{$item->id}}" data-bs-whatever="@getbootstrap">O'zgartirish</button>


                           <div class="modal fade" id="exampleModal{{$item->id}}" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                               <div class="modal-dialog">
                                   <div class="modal-content">
                                       <div class="modal-header">
                                           <h5 class="modal-title" id="exampleModalLabel{{$item->id}}">O'zgartirish</h5>
                                           <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                                       </div>
                                       <div class="modal-body">
                                           <form action="{{route('portfolio.update',$item->id)}}" enctype="multipart/form-data" method="post">
                                               @csrf
                                               @method('put')

                                               <div class="mb-3">
                                                   <label for="image" class="col-form-label">Rasmni o'zgartirish</label>
                                                   <input name="image" type="file"  class="form-control" value="{{$item->image}}" >

                                                   <label for="name" class="col-form-label">Sarlavhani o'zgartirish</label>
                                                   <input name="name" type="text" required class="form-control" value="{{ $item->name }}">


                                                   <label for="text" class="col-form-label"> Textni o'zgartirish</label>
                                                   <input name="text" type="text" required class="form-control" value="{{$item->text}}">

                                                   <label for="text" class="col-form-label">kategoriya tanlang</label>
                                                   <select name="cat_id" class="form-select">
                                                       @foreach($categories as $category)
                                                           <option value="{{$category->id}}">{{$category->name}}</option>
                                                       @endforeach
                                                   </select>
                                               </div>
                                               <div class="modal-footer">
                                                   <button  class="btn btn-secondary" data-bs-dismiss="modal">Bekor qilish</button>
                                                   <button type="submit" class="btn btn-primary">Ozgartirish</button>
                                               </div>
                                           </form>
                                       </div>

                                   </div>
                               </div>
                           </div>

                           <button type="button" class="btn btn-outline-danger" data-bs-toggle="modal" data-bs-target="#delete{{$item->id}}" data-bs-whatever="@getbootstrap">O'chirish</button>

                           <div class="modal fade" id="delete{{$item->id}}" tabindex="-1" aria-labelledby="exampleModalLabel123" aria-hidden="true">
                               <div class="modal-dialog">
                                   <div class="modal-content">
                                       <div class="modal-header">
                                           <h5 class="modal-title" id="exampleModalLabel123">New message</h5>
                                           <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                                       </div>
                                       <div class="modal-body">
                                           <form action="{{route('portfolio.destroy',$item->id)}}" enctype="multipart/form-data" method="post">
                                               @csrf
                                               @method('DELETE')
                                               <div class="modal-body">
                                                   <h3>Ishonchingiz komilmi?</h3>
                                               </div>
                                               <div class="modal-footer">
                                                   <button  class="btn btn-secondary " type="button"  data-bs-dismiss="modal" aria-label="Close">Bekor qilish</button>
                                                   <button type="submit" class="btn btn-danger">O'chirish</button>
                                               </div>
                                           </form>
                                       </div>

                                   </div>
                               </div>
                           </div>
                           </p>
                       </div>
                   </div></div>


           @endforeach


       </div>
   </div>



    @endsection
