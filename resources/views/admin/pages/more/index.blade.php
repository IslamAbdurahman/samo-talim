@extends('admin.layouts.app')



@section('main')


<div class="container">
    <div class="text-center">Qo'shimcha Savollar</div>
    <button type="button" class="btn btn-outline-info col-3 text-center center m-1" data-bs-toggle="modal" data-bs-target="#exampleModal" data-bs-whatever="@getbootstrap">Qo'shish</button>

    <div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Qo'shish</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body ">
                    <form action="{{route('more.store')}}" method="post" enctype="multipart/form-data">
                        @csrf
                        @method('post')
                        <div class="mb-3">


                            <label for="name" class="col-form-label">savolni kiriting</label>
                            <input name="name" type="text" class="form-control" placeholder="savol">

                            <label for="swipe_text" class="col-form-label">javobni kiriting</label>
                            <input name="swipe_text" type="text" class="form-control" placeholder="javob">





                        </div>
                        <div class="modal-footer">
                            <button  class="btn btn-secondary" data-bs-dismiss="modal">Bekor qilish</button>
                            <button type="submit" class="btn btn-primary">Yaratish</button>
                        </div>
                    </form>
                </div>

            </div>
        </div>
    </div>

    <div class="row">
       @foreach($more as $item)
            <div class="modal fade" id="exampleModal{{$item->id}}" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLabel{{$item->id}}">Yangi o'zgarish</h5>
                            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                        </div>
                        <div class="modal-body">
                            <form action="{{route('more.update',$item->id)}}" enctype="multipart/form-data" method="post">
                                @csrf
                                @method('put')

                                <div class="mb-3">
                                    <label for="name" class="col-form-label">savolni o'zgartirish</label>
                                    <input name="name" type="text"  class="form-control" value="{{$item->name}}" >

                                    <label for="swipe_text" class="col-form-label">javobni o'zgartirish</label>
                                    <input name="swipe_text" type="text" required class="form-control" value="{{ $item->swipe_text }}">



                                </div>
                                <div class="modal-footer">
                                    <button  class="btn btn-secondary" data-bs-dismiss="modal">Bekor qilish</button>
                                    <button type="submit" class="btn btn-primary">O'zgartirish</button>
                                </div>
                            </form>
                        </div>

                    </div>
                </div>
            </div>

            <div class="accordion-item">
                <h3 class="accordion-header">
                    <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#faq-content-1{{$item->id}}">

                        {{$item->name}}
                    </button>
                </h3>
                <div id="faq-content-1{{$item->id}}" class="accordion-collapse collapse" data-bs-parent="#faqlist">
                    <div class="accordion-body">
                        {{$item->swipe_text}}
                        <div class="row">
                            <button  type="button" class="btn btn-primary col-3" data-bs-toggle="modal" data-bs-target="#exampleModal{{$item->id}}" data-bs-whatever="@getbootstrap">O'zgartirish</button>

                            <div class="modal fade" id="exampleModal{{$item->id}}" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h5 class="modal-title" id="exampleModalLabel{{$item->id}}">Yangi o'zgarish</h5>
                                            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                                        </div>
                                        <div class="modal-body">
                                            <form action="{{route('more.update',$item->id)}}" enctype="multipart/form-data" method="post">
                                                @csrf
                                                @method('put')

                                                <div class="mb-3">
                                                    <label for="name" class="col-form-label">savolni o'zgartirish</label>
                                                    <input name="name" type="text"  class="form-control" value="{{$item->name}}" >

                                                    <label for="swipe_text" class="col-form-label">javobni o'zgartirish</label>
                                                    <input name="swipe_text" type="text" required class="form-control" value="{{ $item->swipe_text }}">



                                                </div>
                                                <div class="modal-footer">
                                                    <button  class="btn btn-secondary" data-bs-dismiss="modal">Bekor qilish</button>
                                                    <button type="submit" class="btn btn-primary">O'zgartirish</button>
                                                </div>
                                            </form>
                                        </div>

                                    </div>
                                </div>
                            </div>


                            <button type="button" class="btn btn-outline-danger col-3" data-bs-toggle="modal" data-bs-target="#exampleModal{{$item->name}}" data-bs-whatever="@getbootstrap">O'chirish</button>

                            <div class="modal fade" id="exampleModal{{$item->name}}" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h5 class="modal-title" id="exampleModalLabel{{$item->id+1}}">O'chirish</h5>
                                            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                                        </div>
                                        <div class="modal-body">
                                            <form action="{{route('more.destroy',$item->id)}}" enctype="multipart/form-data" method="post">
                                                @csrf
                                                @method('DELETE')
                                                <div class="modal-body">
                                                    <h3>Ishonchingiz komilmi?</h3>
                                                </div>
                                                <div class="modal-footer">
                                                    <button  class="btn btn-secondary" data-bs-dismiss="modal">Bekor qilish</button>
                                                    <button type="submit" class="btn btn-danger">O'chirish</button>
                                                </div>
                                            </form>
                                        </div>

                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div><!-- # Faq item-->

        @endforeach

    </div>
</div>


    @endsection
