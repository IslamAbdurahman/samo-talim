

@extends('admin.layouts.app')


@section('main')

    <div class="row">
        <h2 class="text-center col-6">Card Sozlamalari</h2>
        <div class="col-6">
            <div class="text-end">
                <button type="button" class="btn btn-primary" data-bs-toggle="modal" data-bs-target="#create" data-bs-whatever="@getbootstrap">Card qo'shish</button>

            </div>
            <div class="modal fade" id="create" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLabel">Card</h5>
                            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                        </div>
                        <div class="modal-body">
                            <form action="{{route('cards.store')}}" method="post" enctype="multipart/form-data">
                                @csrf
                                @method('post')
                                <div class="mb-3">
                                    <label for="header_text" class="col-form-label">Yozuvmi Kiriting</label>
                                    <input name="text" type="text" class="form-control">

                                    <label for="img" class="col-form-label">Suratni Kiriting</label>
                                    <input name="image" type="file" class="form-control">

                                    <label for="link" class="col-form-label">Kartning linki</label>
                                    <input name="link" type="text" class="form-control">

                                </div>
                                <div class="modal-footer">
                                    <button type="submit" class="btn btn-secondary" data-bs-dismiss="modal">Bekor qilish</button>
                                    <button type="submit" class="btn btn-primary">Tasdiqlash</button>
                                </div>
                            </form>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>

   <div class="row">
       <div class="">
           <div class="container position-relative">
               <div class="row gy-4 mt-5">
                   @foreach($cards as $card)
                   <div class="col-xl-3 col-md-6" data-aos="fade-up" data-aos-delay="100">
                       <div >
                           <div>
                               <img src="{{ asset('public/storage/images/'.$card->image)  }}" alt="{{ $card->image }}" class="img-fluid rounded-4 mb-4 w-10">
                           </div>
                           <div class="">

                               <h4 class="title"><a>Assosiy Yozuv: {{$card->text}}</a></h4>
                           </div>
                       </div>


                      <div class="d-flex justify-content-between">

{{--                          Update--}}
                          <div class="">
                              <button type="button" class="btn btn-primary" data-bs-toggle="modal" data-bs-target="#update{{$card->id}}" data-bs-whatever="@getbootstrap">Card Tahrirlash</button>
                          </div>
                          <div class="modal fade" id="update{{$card->id}}" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                              <div class="modal-dialog">
                                  <div class="modal-content">
                                      <div class="modal-header">
                                          <h5 class="modal-title" id="exampleModalLabel">Card</h5>
                                          <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                                      </div>
                                      <div class="modal-body">
                                          <form action="{{route('cards.update', $card->id)}}" method="post" enctype="multipart/form-data">
                                              @csrf
                                              @method('put')
                                              <div class="mb-3">
                                                  <label for="header_text" class="col-form-label">Yozuvmi Ozgartirish</label>
                                                  <input name="text" type="text" class="form-control" value="{{$card->text}}">

                                                  <label for="img" class="col-form-label">Suratni Ozgartirish</label>
                                                  <input name="image" type="file" class="form-control" value="{{$card->image}}">

                                                  <label for="link" class="col-form-label">Kartning link</label>
                                                  <input name="link" type="text" class="form-control" value="{{$card->link}}">

                                              </div>
                                              <div class="modal-footer">
                                                  <button type="submit" class="btn btn-secondary" data-bs-dismiss="modal">Yopish</button>
                                                  <button type="submit" class="btn btn-primary">Malumotlarni o'zgartirish</button>
                                              </div>
                                          </form>
                                      </div>

                                  </div>
                              </div>
                          </div>

{{--                          End Update--}}


{{--                          Delete--}}
                          <div class="" >
                              <button type="button" class="btn btn-danger" data-bs-toggle="modal" data-bs-target="#delete{{$card->id}}" data-bs-whatever="@getbootstrap">Card O'chirish</button>
                          </div>
                          <div class=" modal fade" id="delete{{$card->id}}" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                              <div class="modal-dialog">
                                  <div class="modal-content">
                                      <div class="modal-header">
                                          <h5 class="modal-title" id="exampleModalLabel">Card</h5>
                                          <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                                      </div>
                                      <div class="modal-body">
                                          <form action="{{route('cards.destroy', $card->id)}}" method="post" enctype="multipart/form-data">
                                              @csrf
                                              @method('delete')
                                              <h3>
                                                  Rostan ham malumot o'chirilsimi
                                              </h3>
                                              <div class="modal-footer">
                                                  <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Rad etish</button>
                                                  <button type="submit" class="btn btn-primary">Ma'lumotni O'chirish</button>
                                              </div>
                                          </form>
                                      </div>

                                  </div>
                              </div>
                          </div>

{{--                          End delete--}}
                      </div>

                   </div>
                   <!--End Icon Box -->



                   @endforeach

               </div>

           </div>

       </div>
   </div>



@endsection
