@extends('admin.layouts.app')



@section('main')

    <button type="button" class="btn btn-outline-info col-3 text-center center m-1" data-bs-toggle="modal" data-bs-target="#exampleModal" data-bs-whatever="@getbootstrap">Qo'shish</button>

    <div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Yaratish</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body ">
                    <form action="{{route('teachers.store')}}" method="post" enctype="multipart/form-data">
                        @csrf
                        @method('post')
                        <div class="mb-3">
                            <label for="image" class="col-form-label">Rasmni kiriting</label>
                            <input name="image" type="file" class="form-control" >

                            <label for="teacher_name" class="col-form-label">Ismni kiriting</label>
                            <input name="teacher_name" type="text" class="form-control" placeholder="ism">

                            <label for="teacher_job" class="col-form-label">kasbni kiriting</label>
                            <input name="teacher_job" type="text" class="form-control" placeholder="kasb">


                            <label for="twitter" class="col-form-label">twitter manzilini kiriting</label>
                            <input name="twitter" type="text" class="form-control" value="https://twitter.com/">


                            <label for="facebook_icon" class="col-form-label">facebook manzilini kiriting</label>
                            <input name="facebook_icon" type="text" class="form-control" value="https://facebook.com/">


                            <label for="instagram_icon" class="col-form-label">instagram manzilini kiriting</label>
                            <input name="instagram_icon" type="text" class="form-control" value="https://instagram.com/">


                            <label for="telegram_icon" class="col-form-label">twitter manzilini kiriting</label>
                            <input name="telegram_icon" type="text" class="form-control" value="https://t.me/">


                        </div>
                        <div class="modal-footer">
                            <button  class="btn btn-secondary" data-bs-dismiss="modal">Bekor qilish</button>
                            <button type="submit" class="btn btn-primary">Yaratish</button>
                        </div>
                    </form>
                </div>

            </div>
        </div>
    </div>



    <div class="row">
        @foreach($teachers as $item)

            <div class="modal fade" id="exampleModal{{$item->id}}" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLabel{{$item->id}}">Yangi o'zgarish</h5>
                            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                        </div>
                        <div class="modal-body">
                            <form action="{{route('teachers.update',$item->id)}}" enctype="multipart/form-data" method="post">
                                @csrf
                                @method('put')

                                <div class="mb-3">
                                    <label for="image" class="col-form-label">Rasmni o'zgartirish</label>
                                    <input name="image" type="file"  class="form-control" value="{{$item->image}}" >

                                    <label for="teacher_name" class="col-form-label">ismni o'zgartirish</label>
                                    <input name="teacher_name" type="text" required class="form-control" value="{{ $item->teacher_name }}">


                                    <label for="teacher_job" class="col-form-label"> kasbni o'zgartirish</label>
                                    <input name="teacher_job" type="text" required class="form-control" value="{{$item->teacher_name}}">

                                    <label for="twitter" class="col-form-label"> twitter linkini o'zgartirish</label>
                                    <input name="twitter" type="text" required class="form-control" value="{{$item->twitter}}">

                                    <label for="facebook_icon" class="col-form-label"> facebook linkini o'zgartirish</label>
                                    <input name="facebook_icon" type="text" required class="form-control" value="{{$item->facebook_icon}}">

                                    <label for="instagram_icon" class="col-form-label"> instagram linkini o'zgartirish</label>
                                    <input name="instagram_icon" type="text" required class="form-control" value="{{$item->instagram_icon}}">

                                    <label for="telegram_icon" class="col-form-label"> telegram linkini o'zgartirish</label>
                                    <input name="telegram_icon" type="text" required class="form-control" value="{{$item->telegram_icon}}">
                                </div>
                                <div class="modal-footer">
                                    <button  class="btn btn-secondary" data-bs-dismiss="modal">Bekor qilish</button>
                                    <button type="submit" class="btn btn-primary">O'zgartirish</button>
                                </div>
                            </form>
                        </div>

                    </div>
                </div>
            </div>


            <div class="col-3">
                <div class="card" style="width: 100%;">
                    <img class="card-img-top" width="300px" height="250px" src="{{ asset('public/storage/images/'.$item->image)  }}" alt="{{$item->image}}">
                    <div class="card-body">
                        <h5 class="card-title">{{$item->teacher_name}}</h5>
                        <p class="card-text">{{$item->teacher_job}}</p>
                        <div class="row w-100 social d-flex justify-content-around float-start">

                            <a class="col-3" href="{{$item->twitter}}"><i class="bi bi-twitter "></i></a>
                            <a class="col-3" href="{{$item->facebook_icon}}"><i class="bi bi-facebook"></i></a>
                            <a class="col-3" href="{{$item->instagram_icon}}"><i class="bi bi-instagram"></i></a>
                            <a class="col-3" href="{{$item->telegram_icon}}"><i class="bi bi-telegram"></i></a>
                        </div>

                        <div class="row col-12 center">
                            <button type="button" class="btn btn-primary" data-bs-toggle="modal" data-bs-target="#exampleModal{{$item->id}}" data-bs-whatever="@getbootstrap">O'zgartirish</button>



                            <button type="button" class="btn btn-outline-danger" data-bs-toggle="modal" data-bs-target="#exampleModaldelete{{$item->id}}" data-bs-whatever="@getbootstrap">O'chirish</button>

                            <div class="modal fade" id="exampleModaldelete{{$item->id}}" tabindex="-1" aria-labelledby="exampleModalLabeldelete" aria-hidden="true">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h5 class="modal-title" id="exampleModalLabeldelete{{$item->id}}">O'chirish</h5>
                                            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                                        </div>
                                        <div class="modal-body">
                                            <form action="{{route('teachers.destroy',$item->id)}}" enctype="multipart/form-data" method="post">
                                                @csrf
                                                @method('DELETE')
                                                <div class="modal-body">
                                                    <h3>Ishonchingiz komilmi?</h3>
                                                </div>
                                                <div class="modal-footer">
                                                    <button  class="btn btn-secondary " type="button"  data-bs-dismiss="modal" aria-label="Close">Bekor qilish</button>
                                                    <button type="submit" class="btn btn-danger">O'chirish</button>
                                                </div>
                                            </form>
                                        </div>

                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>


        @endforeach
    </div>



@endsection
