

    @extends('admin.layouts.app')


@section('main')

        <!DOCTYPE html>
    <html lang="en">

    <head>
        <meta charset="utf-8">
        <meta content="width=device-width, initial-scale=1.0" name="viewport">

        <title>Impact Bootstrap Template - Index</title>
        <meta content="" name="description">
        <meta content="" name="keywords">

        <!-- Favicons -->
        <link href="public/samo/assets/img/favicon.png" rel="icon">
        <link href="public/samo/assets/img/apple-touch-icon.png" rel="apple-touch-icon">

        <!-- Google Fonts -->
        <link rel="preconnect" href="https://fonts.googleapis.com">
        <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
        <link href="https://fonts.googleapis.com/css2?family=Open+Sans:ital,wght@0,300;0,400;0,500;0,600;0,700;1,300;1,400;1,600;1,700&family=Montserrat:ital,wght@0,300;0,400;0,500;0,600;0,700;1,300;1,400;1,500;1,600;1,700&family=Raleway:ital,wght@0,300;0,400;0,500;0,600;0,700;1,300;1,400;1,500;1,600;1,700&display=swap" rel="stylesheet">

        <!-- Vendor CSS Files -->
        <link href="public/samo/assets/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
        <link href="public/samo/assets/vendor/bootstrap-icons/bootstrap-icons.css" rel="stylesheet">
        <link href="public/samo/assets/vendor/aos/aos.css" rel="stylesheet">
        <link href="public/samo/assets/vendor/glightbox/css/glightbox.min.css" rel="stylesheet">
        <link href="public/samo/assets/vendor/swiper/swiper-bundle.min.css" rel="stylesheet">

        <!-- Template Main CSS File -->
        <link href="public/samo/assets/css/main.css" rel="stylesheet">

        <!-- =======================================================
        * Template Name: Impact - v1.1.1
        * Template URL: https://bootstrapmade.com/impact-bootstrap-business-website-template/
        * Author: BootstrapMade.com
        * License: https://bootstrapmade.com/license/
        ======================================================== -->
    </head>

    <body>


    <div>
        <div class="text-center">
            <section id="call-to-action" class="call-to-action ">
                <div class="container text-center" data-aos="zoom-out">
                    <a href="{{$intro->video}}" class="glightbox play-btn"></a>
                    <h3>{{$intro->name}}</h3>
                    <p> {{$intro->text}}</p>
                    <a class="cta-btn" href="#">Biz bilan aloqa</a>
                </div>
                {{--        Update--}}
                <div class="mt-5">
                    <button type="button" class="btn btn-primary" data-bs-toggle="modal" data-bs-target="#update{{$intro->id}}" data-bs-whatever="@getbootstrap">Ma'lumotlarni Tahrirlash</button>
                </div>
                <div class="modal fade" id="update{{$intro->id}}" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title" id="exampleModalLabel">Ma'lumotlarni Tahrirlash</h5>
                                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                            </div>
                            <div class="modal-body">
                                <form action="{{route('introduction.update', $intro->id)}}" method="post" enctype="multipart/form-data">
                                    @csrf
                                    @method('put')
                                    <div class="mb-3">

                                        <label for="name" class="form-label">Asosiy yozuv</label>
                                        <input type="text" name="name" class="form-control" value="{{$intro->name}}">

                                        <label for="name" class="form-label">Oddiy yozuv</label>
                                        <input type="text" name="text" class="form-control" value="{{$intro->text}}">

                                        <label for="name" class="form-label">Video link</label>
                                        <input type="text" name="video" class="form-control" value="{{$intro->video}}">

                                    </div>
                                    <div class="modal-footer">
                                        <button type="submit" class="btn btn-secondary" data-bs-dismiss="modal">Yopish</button>
                                        <button type="submit" class="btn btn-primary">Malumotlarni o'zgartirish</button>
                                    </div>
                                </form>
                            </div>

                        </div>
                    </div>
                </div>

                {{--                          End Update--}}
            </section><!-- End Call To Action Section -->

        </div>


    </div>




    <!-- Vendor JS Files -->
    <script src="public/samo/assets/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
    <script src="public/samo/assets/vendor/aos/aos.js"></script>
    <script src="public/samo/assets/vendor/glightbox/js/glightbox.min.js"></script>
    <script src="public/samo/assets/vendor/purecounter/purecounter_vanilla.js"></script>
    <script src="public/samo/assets/vendor/swiper/swiper-bundle.min.js"></script>
    <script src="public/samo/assets/vendor/isotope-layout/isotope.pkgd.min.js"></script>
    <script src="public/samo/assets/vendor/php-email-form/validate.js"></script>

    <!-- Template Main JS File -->
    <script src="public/samo/assets/js/main.js"></script>
    </body>

</html>


@endsection
