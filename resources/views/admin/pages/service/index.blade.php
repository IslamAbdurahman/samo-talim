@extends('admin.layouts.app')


@section('main')

    <div class="container">
        <div class="row">
            <div class="col-6">{{ $service->image }}</div>
            <div class="col-6">
                <div class="card" style="width: 18rem;">
                    <ul class="list-group list-group-flush">
                        <li class="list-group-item">{{$service->number}} {{$service->bold_text}} {{$service->simple_text}}</li>

                    </ul>
                </div>
            </div>
        </div>
    </div>

@endsection
