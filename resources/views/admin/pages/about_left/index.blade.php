@extends('admin.layouts.app')

@section("main")



<div class="container text-center">



    <div class="section-header">
        <h2>Biz haqimizda</h2>
    </div>

    <div class="row gy-4">

        <div class="col-lg-3">

            <h3>{{$about_left->header_text}}</h3>
            <img src="{{ asset('public/storage/images/'.$about_left->image)  }}" alt="{{ $about_left->image }}" class="img-fluid rounded-4 mb-4 about_img">
            <p>{{$about_left->text_bottom}}</p>

            <button type="button" class="btn btn-primary" data-bs-toggle="modal" data-bs-target="#exampleModal" data-bs-whatever="@getbootstrap">Ma'lumotlarni tahrirlash</button>

            <div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLabel">Ma'lumotlarni tahrirlash</h5>
                            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                        </div>
                        <div class="modal-body">
                            <form action="{{route('about_left.update', $about_left->id)}}" method="post" enctype="multipart/form-data">
                                @csrf
                                @method('put')
                                <div class="mb-3">
                                    <label for="header_text" class="col-form-label">Asosiy yozuvni Ozgartirish</label>
                                    <input name="header_text" type="text" class="form-control" value="{{$about_left->header_text}}">

                                    <label for="img" class="col-form-label">Suratni Ozgartirish</label>
                                    <input name="image" type="file" class="form-control" value="{{$about_left->image}}">

                                    <label for="text_bottom" class="col-form-label">Yozuvmi Ozgartirish</label>
                                    <input name="text_bottom" type="text" class="form-control" value="{{$about_left->text_bottom}}">
                                </div>
                                <div class="modal-footer">
                                    <button type="submit" class="btn btn-secondary" data-bs-dismiss="modal">Yopish</button>
                                    <button type="submit" class="btn btn-primary">O'zgarisglarni saqlash</button>
                                </div>
                            </form>
                        </div>

                    </div>
                </div>
            </div>
        </div>

        <div class="col-lg-3">
            <div >
                <p class="fst-italic" style="text-overflow:ellipsis; ">
                   {{$about_right->italic_text}}
                </p>
                <ul style="text-overflow: ellipsis; " >
                    <li><i class="bi bi-check-circle-fill"></i>{{$about_right->check_text}}</li>
                </ul>
                <p style="text-overflow: ellipsis; ">
                    {{$about_right->simple_text}}
                </p>
                <p style="text-overflow: ellipsis; ">
                    {{$about_right->video}}
                </p>

                <div class="position-relative mt-4">
                    <img src="{{asset('public/storage/images/'.$about_right->img_bg)}}" class="img-fluid rounded-4" alt="{{$about_right->img_bg}}">
                </div>
            </div>
            <button type="button" class="btn btn-primary" data-bs-toggle="modal" data-bs-target="#Modal" data-bs-whatever="@getbootstrap">Ma'lumotlarni tahrirlash</button>

            <div class="modal fade" id="Modal" tabindex="-1" aria-labelledby="ModalLabel" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="ModalLabel">Ma'lumotlarni tahrirlash</h5>
                            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                        </div>
                        <div class="modal-body">
                            <form action="{{route('about_right.update',$about_right->id)}}" method="post" enctype="multipart/form-data">
                                @csrf
                                @method('put')
                                <div class="mb-3">
                                    <label for="header_text" class="col-form-label"> Yozuvni Ozgartirish</label>
                                    <input name="italic_text" type="text" class="form-control" value="{{$about_right->italic_text}}">

                                    <label for="img" class="col-form-label">Yozuvni Ozgartirish</label>
                                    <input name="check_text" type="text" class="form-control" value="{{$about_right->check_text}}">

                                    <label for="text_bottom" class="col-form-label">Yozuvmi Ozgartirish</label>
                                    <input name="simple_text" type="text" class="form-control" value="{{$about_right->simple_text}}">

                                    <label for="text_bottom" class="col-form-label">Video linkini o'zgartirish</label>
                                    <input name="video" type="text" class="form-control" value="{{$about_right->video}}">

                                    <label for="text_bottom" class="col-form-label">Suratni o'zgartirih</label>
                                    <input name="img_bg" type="file" class="form-control" value="{{$about_right->img_bg}}">
                                </div>
                                <div class="modal-footer">
                                    <button type="submit" class="btn btn-secondary" data-bs-dismiss="modal">Yopish</button>
                                    <button type="submit" class="btn btn-primary">O'zgarishlarni saqlash</button>
                                </div>
                            </form>
                        </div>

                    </div>
                </div>
            </div>
        </div>

    </div>






@endsection
