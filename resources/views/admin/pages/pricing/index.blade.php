@extends('admin.layouts.app')



@section("main")


  <div class="container">
      <div class="row">
          <div class="col-4">
              <div class="card bg-light mb-3" style="max-width: 18rem;">
                  <div class="card-header">
                      <div class="icon center text-center">
                          <i class="bi bi-box"></i>
                      </div>
                  </div>
                  <div class="card-body">
                      <h5 class="card-title">{{ $pricing[0]->name }}</h5>
                      <h1 class="card-text text-center center">{{ $pricing[0]->price_number }}<sup>so'm</sup>/oy</h1>

                          <p class="card-text"><i class="bi bi-check"></i>{{ $pricing[0]->correct_text }}</p>
                          <p class="card-text text-decoration-line-through"><i class="bi bi-x"></i>{{ $pricing[0]->incorrect_text }}</p>

                        <h3>
                            <button type="button" class="btn btn-primary" data-bs-toggle="modal" data-bs-target="#exampleModal" data-bs-whatever="@getbootstrap">O'zgartirish</button>

                            <div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h5 class="modal-title" id="exampleModalLabel">Yangi o'zgarish</h5>
                                            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                                        </div>
                                        <div class="modal-body">
                                            <form action="{{route('pricing.update', $pricing[0]->id)}}" method="post" enctype="multipart/form-data">
                                                @csrf
                                                @method('put')
                                                <div class="mb-3">
                                                    <label for="name" class="col-form-label">Nomni o'zgartirish</label>
                                                    <input name="name" type="text" required class="form-control" value="{{$pricing[0]->name}}">

{{--                                                    <label for="image" class="col-form-label">Rasmni pochtani o'zgartirish</label>--}}
{{--                                                    <input name="image" type="text" required class="form-control" value="{{$pricing[0]->image}}">--}}

                                                    <label for="price_number" class="col-form-label"> Raqamni o'zgartirish</label>
                                                    <input name="price_number" type="number" required class="form-control" value="{{$pricing[0]->price_number}}">

                                                    <label for="correct_text" class="col-form-label"> Bor imkoniyatlar</label>
                                                    <input name="correct_text" type="text" required class="form-control" value="{{$pricing[0]->correct_text}}">




                                                    <label for="incorrect_text" class="col-form-label">Yo'q imkoniyatlar</label>
                                                    <input name="incorrect_text" type="text" required class="form-control" value="{{$pricing[0]->incorrect_text}}">
                                                </div>
                                                <div class="modal-footer">
                                                    <button  class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                                                    <button type="submit" class="btn btn-primary">Yangilash</button>
                                                </div>
                                            </form>
                                        </div>

                                    </div>
                                </div>
                            </div>

                        </h3>
                  </div>
              </div>
          </div>

          <div class="col-4">
              <div class="card bg-light mb-3" style="max-width: 18rem;">
                  <div class="card-header">
                      <div class="icon center text-center">
                          <i class="bi bi-airplane"></i>
                      </div>
                  </div>
                  <div class="card-body">
                      <h5 class="card-title">{{ $pricing[1]->name }}</h5>
                      <h1 class="card-text text-center center">{{ $pricing[1]->price_number }}<sup>so'm</sup>/oy</h1>
                      <p class="card-text"><i class="bi bi-check"></i>{{ $pricing[1]->correct_text }}</p>
                      <p class="card-text text-decoration-line-through"><i class="bi bi-x"></i>{{ $pricing[1]->incorrect_text }}</p>
                      <h3>
                          <button type="button" class="btn btn-primary" data-bs-toggle="modal" data-bs-target="#exampleModal1" data-bs-whatever="@getbootstrap">O'zgartirish</button>

                          <div class="modal fade" id="exampleModal1" tabindex="-1" aria-labelledby="exampleModalLabel1" aria-hidden="true">
                              <div class="modal-dialog">
                                  <div class="modal-content">
                                      <div class="modal-header">
                                          <h5 class="modal-title" id="exampleModalLabel1">Yangi o'zgarish</h5>
                                          <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                                      </div>
                                      <div class="modal-body">
                                          <form action="{{route('pricing.update', $pricing[1]->id)}}" method="post" enctype="multipart/form-data">
                                              @csrf
                                              @method('put')
                                              <div class="mb-3">
                                                  <label for="name" class="col-form-label">Nomni o'zgartirish</label>
                                                  <input name="name" type="text" class="form-control" value="{{$pricing[1]->name}}">

                                                  {{--                                                  <label for="image" class="col-form-label">Rasmni pochtani o'zgartirish</label>--}}
                                                  {{--                                                  <input name="image" type="text" class="form-control" value="{{$pricing[1]->image}}">--}}

                                                  <label for="price_number" class="col-form-label"> Raqamni o'zgartirish</label>
                                                  <input name="price_number" type="number" class="form-control" value="{{$pricing[1]->price_number}}">

                                                  <label for="correct_text" class="col-form-label"> Bor imkoniyatlar</label>
                                                  <input name="correct_text" type="text" class="form-control" value="{{$pricing[1]->correct_text}}">

                                                  <label for="incorrect_text" class="col-form-label">Yo'q imkoniyatlar</label>
                                                  <input name="incorrect_text" type="text" class="form-control" value="{{$pricing[1]->incorrect_text}}">
                                              </div>
                                              <div class="modal-footer">
                                                  <button  class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                                                  <button type="submit" class="btn btn-primary">Yangilash</button>
                                              </div>
                                          </form>
                                      </div>

                                  </div>
                              </div>
                          </div>

                      </h3>
                  </div>
              </div>
          </div>

          <div class="col-4">
              <div class="card bg-light mb-3" style="max-width: 18rem;">
                  <div class="card-header">
                      <div class="icon center text-center">
                          <i class="bi bi-send"></i>
                      </div>
                  </div>
                  <div class="card-body">
                      <h5 class="card-title">{{ $pricing[2]->name }}</h5>
                      <h1 class="card-text text-center center">{{ $pricing[2]->price_number }}<sup>so'm</sup>/oy</h1>
                      <p class="card-text"><i class="bi bi-check"></i>{{ $pricing[2]->correct_text }}</p>
                      <p class="card-text text-decoration-line-through"><i class="bi bi-x"></i>{{ $pricing[2]->incorrect_text }}</p>
                      <h3>
                          <button type="button" class="btn btn-primary" data-bs-toggle="modal" data-bs-target="#exampleModal2" data-bs-whatever="@getbootstrap">O'zgartirish</button>

                          <div class="modal fade" id="exampleModal2" tabindex="-1" aria-labelledby="exampleModalLabel2" aria-hidden="true">
                              <div class="modal-dialog">
                                  <div class="modal-content">
                                      <div class="modal-header">
                                          <h5 class="modal-title" id="exampleModalLabel2">Yangi o'zgarish</h5>
                                          <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                                      </div>
                                      <div class="modal-body">
                                          <form action="{{route('pricing.update', $pricing[2]->id)}}" method="post" enctype="multipart/form-data">
                                              @csrf
                                              @method('put')
                                              <div class="mb-3">
                                                  <label for="name" class="col-form-label">Nomni o'zgartirish</label>
                                                  <input name="name" type="text" class="form-control" value="{{$pricing[2]->name}}">

                                                  {{--                                                  <label for="image" class="col-form-label">Rasmni pochtani o'zgartirish</label>--}}
                                                  {{--                                                  <input name="image" type="text" class="form-control" value="{{$pricing[2]->image}}">--}}

                                                  <label for="price_number" class="col-form-label"> Raqamni o'zgartirish</label>
                                                  <input name="price_number" type="number" class="form-control" value="{{$pricing[2]->price_number}}">

                                                  <label for="correct_text" class="col-form-label"> Bor imkoniyatlar</label>
                                                  <input name="correct_text" type="text" class="form-control" value="{{$pricing[2]->correct_text}}">

                                                  <label for="incorrect_text" class="col-form-label">Yo'q imkoniyatlar</label>
                                                  <input name="incorrect_text" type="text" class="form-control" value="{{$pricing[2]->incorrect_text}}">
                                              </div>
                                              <div class="modal-footer">
                                                  <button  class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                                                  <button type="submit" class="btn btn-primary">Yangilash</button>
                                              </div>
                                          </form>
                                      </div>

                                  </div>
                              </div>
                          </div>

                      </h3>
                  </div>
              </div>
          </div>
      </div>
  </div>



@endsection




